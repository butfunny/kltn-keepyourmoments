var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var Caman = require('caman').Caman;
var easyimg = require('easyimage');



Caman.Filter.register("pleasant", function() {
    this.colorize(60, 105, 218, 10);
    this.contrast(10);
    this.sunrise();
    this.hazyDays();
    this.render();
});
//
//easyimg.convert({src:'./img/IMG_1270.JPG', dst:'./img/beach.png', quality:10}, function(err, stdout, stderr) {
//    if (err) throw err;
//    console.log('Converted JPG to PNG, quality set at 10/100');
//});

Caman("./img/beach.PNG", function () {
    this.pleasant();
    var start = new Date().getTime();


    this.render(function () {
        console.log(new Date().getTime() - start ) ;
        this.save("./img/output.png");
    });
});

app.listen(3000, function () {
    console.log("server running in port: 3000");
});