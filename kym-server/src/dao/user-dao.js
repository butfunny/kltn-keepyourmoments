var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
    user_id: String,
    facebook_id: String,
    name: String,
    email: String,
    admin: Boolean,
    username: String,
    password: String,
    avatar: {type: Boolean, default: false},
    avatar_url: String,
    sex: String,
    friends: [String],
    created: {type: Date, default: Date.now}
}, "user");