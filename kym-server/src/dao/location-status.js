var mongoose = require('mongoose');

module.exports = mongoose.model('locationStatus', {
    location_id: String,
    image: String,
    text: String,
    created: {type: Date, default: Date.now}
}, "location_status");