var mongoose = require('mongoose');

module.exports = mongoose.model('LocationLike', {
    user_id: String,
    status_location_id: String,
    admin: Boolean,
    created: {type: Date, default: Date.now}
}, "like_location");