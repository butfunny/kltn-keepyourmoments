var mongoose = require('mongoose');

module.exports = mongoose.model('LocationRequest', {
    owned: String,
    name: String,
    pos: {
        lat: String,
        lng: String
    },
    info: String,
    status : String,
    reason: String,
    created: {type: Date, default: Date.now}
}, "location_request");