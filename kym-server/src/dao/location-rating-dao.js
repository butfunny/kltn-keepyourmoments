var mongoose = require('mongoose');

module.exports = mongoose.model('LocationRating', {
    user_id: String,
    location_id: String,
    rate_number: String,
    created: {type: Date, default: Date.now}
}, "location_rating");