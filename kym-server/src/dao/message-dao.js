var mongoose = require('mongoose');

module.exports = mongoose.model('Message', {
    session_id: String,
    user_id: String,
    text: String,
    image: String,
    created: {type: Date, default: Date.now}
}, "message");