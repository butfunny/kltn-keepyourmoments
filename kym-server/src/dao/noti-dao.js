var mongoose = require('mongoose');

module.exports = mongoose.model('Notification', {
    owned: String,
    type: String,
    params: Object,
    seen: {type: Boolean, default: false},
    created: {type: Date, default: Date.now}

}, "notification");