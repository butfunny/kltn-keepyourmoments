var mongoose = require('mongoose');

module.exports = mongoose.model('LocationAds', {
    location_id: String,
    status_location_id: String,
    created: {type: Date, default: Date.now}
}, "location_ads");