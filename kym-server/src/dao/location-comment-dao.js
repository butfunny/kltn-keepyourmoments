var mongoose = require('mongoose');

module.exports = mongoose.model('CommentLocation', {
    user_id: String,
    status_location_id: String,
    text: String,
    admin: Boolean,
    created: {type: Date, default: Date.now}
}, "comment_location");