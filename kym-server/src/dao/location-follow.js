var mongoose = require('mongoose');

module.exports = mongoose.model('LocationFollow', {
    location_id: String,
    user_follow: String,
    created: {type: Date, default: Date.now}
}, "location_follow");