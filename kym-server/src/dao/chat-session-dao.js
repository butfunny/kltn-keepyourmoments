var mongoose = require('mongoose');

module.exports = mongoose.model('ChatSession', {
    users_owned: [String],
    user1_seen: Date,
    user2_seen: Date,
    created: {type: Date, default: Date.now}
}, "chat_session");