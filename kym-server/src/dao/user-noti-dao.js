var mongoose = require('mongoose');

module.exports = mongoose.model('UserNotification', {
    notification_id: String,
    user_id: String
}, "user_notification");