var mongoose = require('mongoose');

module.exports = mongoose.model('Comment', {
    user_id: String,
    status_id: String,
    text: String,
    created: {type: Date, default: Date.now}
}, "comment");