var mongoose = require('mongoose');

module.exports = mongoose.model('Status', {
    user_id: String,
    image: String,
    text: String,
    faces: String,
    location: String,
    created: {type: Date, default: Date.now}
}, "status");