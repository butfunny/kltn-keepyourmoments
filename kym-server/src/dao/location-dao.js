var mongoose = require('mongoose');

module.exports = mongoose.model('Location', {
    owned: String,
    name: String,
    pos: {
        lat: String,
        lng: String
    },
    avatar: String,
    info: String,
    created: {type: Date, default: Date.now}
}, "location");