var mongoose = require('mongoose');

module.exports = mongoose.model('DetectFace', {
    user_id: String,
    faces: [String]
}, "detect_face");