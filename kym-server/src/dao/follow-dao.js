var mongoose = require('mongoose');

module.exports = mongoose.model('Follow', {
    user_id: String,
    user_to_follow: String,
    created: {type: Date, default: Date.now}
}, "follow");