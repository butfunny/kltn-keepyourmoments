var mongoose = require('mongoose');

module.exports = mongoose.model('Like', {
    user_id: String,
    status_id: String,
    created: {type: Date, default: Date.now}
}, "like");