module.exports = {
    startServer: function(){

        var express = require('express');
        var app = express();
        var bodyParser = require('body-parser');

        var http = require('http').Server(app);
        var io = require('socket.io')(http);


        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));

        app.use(function (req, res, next) {
            res.set("Access-Control-Allow-Origin" , "*");
            res.set("Access-Control-Allow-Credentials", "true");
            res.set("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE");
            res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, set-cookie, Authorization");
            res.set("Access-Control-Expose-Headers", "Set-Cookie");
            next();
        });
        app.use("/uploads", function(req, res, next) {
            res.set("Content-Type", "image/jpeg");
            next();
        });

        app.use('/uploads/image-draft', express.static("./uploads/image-draft"));
        app.use(express.static('./../kym-client/www'));
        app.use('/uploads/avatar', express.static("./uploads/avatar"));

        require("./server/controllers/controllers")(app, io);

        var port = 3000;

        http.listen(port, function () {
            console.log("Server running in port: " + port);
        });


    }
};