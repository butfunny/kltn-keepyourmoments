var Comment = require('./../../dao/comment-dao');
var User = require('./../../dao/user-dao');
var Status = require('./../../dao/status-dao');
var Security = require('../security/security');
var NotificationSupport = require('./../notification/notification-support');

var _ = require('lodash');



module.exports = function (router, io) {

    router.post("/comment",Security.authorDetails, function (req, res) {
        req.body.user_id = req.user._id;
        Comment.create(req.body, function (err, comment) {
            Status.findOne({_id: req.body.status_id}, function (err, s) {
                var noti = {
                    owned: req.user._id,
                    type: "comment",
                    params: {status_id : req.body.status_id}
                };
                if (s.user_id != req.user._id) {
                    NotificationSupport.sendNoti(io, noti, [s.user_id]);
                    io.emit('comment:new', comment);
                    res.send(comment);
                } else {
                    Comment.find({status_id: req.body.status_id}, function (err, comments) {
                        var commentFiltered = _.filter(comments, function (c) {
                            return c.user_id != req.user._id;
                        });
                        var userIds = _.map(commentFiltered, "user_id");
                        NotificationSupport.sendNoti(io, noti, _.uniq(userIds));
                        io.emit('comment:new', comment);
                        res.send(comment);
                    })
                }
            });

        })
    });

    router.delete("/comment/:cid", Security.authorDetails, function (req, res) {
        Comment.findOneAndRemove({_id: req.params.cid}, function (err, comment) {
            io.emit('comment:delete', comment);
            res.end();
        })
    });

    router.post("/comment/prepare", Security.authorDetails, function (req, res) {
        Comment.find({status_id: {$in: req.body.status_ids}}, function (err, comments) {
            res.send(comments);
        })
    });



};