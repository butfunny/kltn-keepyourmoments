var User = require('./../../dao/user-dao');
var Security = require('../security/security');
var _ = require('lodash');
var ChatSession = require('./../../dao/chat-session-dao');
var Message = require('./../../dao/message-dao');



module.exports = function (router, io) {
    router.get("/message/session/me", Security.authorDetails, function (req, res) {
        ChatSession.find({users_owned: req.user._id}, function (err, sessions) {
            res.json(sessions);
        })
    });

    router.post("/message/message-by-session-ids", function (req, res) {
        Message.find({session_id: {$in: req.body}}, function (err, messages) {
            res.send(messages);
        })
    });


    router.post("/message/send", Security.authorDetails, function (req, res) {
        var userSessions = [req.user._id,req.body.to_user];
        ChatSession.findOne({users_owned: userSessions}, function (err, session) {
            if (session == null) {
                ChatSession.findOne({users_owned: userSessions.reverse()}, function (err, anotherSesison) {
                    if (anotherSesison == null) {
                        ChatSession.create({users_owned: userSessions, user1_seen: new Date()}, function (err, newSession) {
                            io.to('user:' + req.user._id).emit('session:new', newSession);
                            io.to('user:' + req.body.to_user).emit('session:new', newSession);
                            Message.create({
                                session_id: newSession._id,
                                user_id: req.user._id,
                                text: req.body.text
                            }, function (err, message) {
                                io.to('user:' + req.user._id).emit('message:new', message);
                                io.to('user:' + req.body.to_user).emit('message:new', message);
                                res.send({session_id : newSession._id});
                            })
                        })
                    } else {
                        var index = _.findIndex(anotherSesison.users_owned, function (u) {return u == req.user._id});
                        if (index == 0) {
                            ChatSession.update({_id : anotherSesison._id}, {$set: {user1_seen: new Date()}}, function () {});
                        } else {
                            ChatSession.update({_id : anotherSesison._id}, {$set: {user2_seen: new Date()}}, function () {});
                        }
                        Message.create({
                            session_id: anotherSesison._id,
                            user_id: req.user._id,
                            text: req.body.text
                        }, function (err, message) {
                            io.to('user:' + req.user._id).emit('message:new', message);
                            io.to('user:' + req.body.to_user).emit('message:new', message);
                            res.send({session_id : anotherSesison._id});
                        })
                    }
                });
            } else {
                var index = _.findIndex(session.users_owned, function (u) {return u == req.user._id});
                if (index == 0) {
                    ChatSession.update({_id : session._id}, {$set: {user1_seen: new Date()}}, function () {});
                } else {
                    ChatSession.update({_id : session._id}, {$set: {user2_seen: new Date()}}, function () {});
                }
                Message.create({
                    session_id: session._id,
                    user_id: req.user._id,
                    text: req.body.text
                }, function (err, message) {
                    io.to('user:' + req.user._id).emit('message:new', message);
                    io.to('user:' + req.body.to_user).emit('message:new', message);
                    res.send({session_id : session._id});
                })
            }
        })
    });

    router.put("/message/seen/:sid", Security.authorDetails, function (req, res) {
        ChatSession.findOne({_id: req.params.sid}, function (err, session) {
            var index = _.findIndex(session.users_owned, function (u) {return u == req.user._id});
            if (index == 0) {
                ChatSession.findOneAndUpdate({_id : session._id}, {$set: {user1_seen: new Date()}}, function (err, sessionModified) {
                    io.to('user:' + session.users_owned[0]).emit('message:seen', sessionModified);
                    io.to('user:' + session.users_owned[1]).emit('message:seen', sessionModified);
                    res.end();
                });
            } else {
                ChatSession.findOneAndUpdate({_id : session._id}, {$set: {user2_seen: new Date()}}, function (err, sessionModified) {
                    io.to('user:' + session.users_owned[1]).emit('message:seen', sessionModified);
                    io.to('user:' + session.users_owned[0]).emit('message:seen', sessionModified);
                    res.end();
                });
            }
        })
    })
};