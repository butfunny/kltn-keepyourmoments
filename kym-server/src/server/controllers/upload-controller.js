var $q = require('q');
var easyimg = require('easyimage');
var multer = require('multer');
var Security = require('./../security/security');
var fs = require('fs');
var Filter = require('../../filter/filter');
var DetectAndRecognizeFace = require('../../dectect-recognize-face/dectect-recognize-face');
var DetectFaceDao = require('../../dao/detect-face-dao');
var _ = require('lodash');

module.exports = function (router, io) {

    var convertToPng = function (file, user_id) {
        var defer = $q.defer();
        easyimg.convert({src:'./uploads/image-draft/' + file, dst:'./uploads/image-draft/' + user_id + '_origin.png', quality:100}).then(
            function(err, stdout, stderr) {
                fs.unlinkSync('./uploads/image-draft/' + file);
                defer.resolve();
            }
        );
        return defer.promise;
    };

    var detectImage = function (req) {
        var defer = $q.defer();
        DetectAndRecognizeFace.detect('./uploads/image-draft/' + req.user._id + '_origin.png').then(function(faces) {
            var faceDetected = JSON.stringify(faces);
            var points = [];
            for (var i = 0; i < faces.images[0].faces.length; i++) {
                var face = faces.images[0].faces[i];
                var point = {
                    widthImage : faces.images[0].width,
                    heightImage : faces.images[0].height,
                    topLeftX: face.topLeftX,
                    topLeftY: face.topLeftY,
                    width: face.width,
                    height: face.height
                };
                points.push(point);
            }

            io.to('user:' + req.user._id).emit("face:detected", points);
            DetectFaceDao.findOne({user_id: req.user._id}, function (err, doc) {
                if (doc != null) {
                    DetectFaceDao.update({user_id: req.user._id}, {$set: {faces: faceDetected}}, function (err) {
                        defer.resolve();
                    })
                } else {
                    DetectFaceDao.create({user_id: req.user._id, faces: faceDetected}, function (err) {
                        defer.resolve();
                    })
                }
            })
        });
        return defer.promise;
    };

    var viewSubject = function (req) {
        var defer = $q.defer();
        DetectFaceDao.findOne({user_id: req.user._id}, function (err, doc) {
            var faces = JSON.parse(doc.faces).images[0].faces;
            var promises = [];
            for (var i = 0; i < faces.length; i++) {
                var face = faces[i];
                promises.push(DetectAndRecognizeFace.recognize(JSON.parse(doc.faces).images[0].image_id, face.leftEyeCenterX, face.leftEyeCenterY, face.rightEyeCenterX, face.rightEyeCenterY));
            }
            $q.all(promises).then(
                function (respRecognize) {
                    for (var i = 0; i < respRecognize.length; i++) {
                        var respReg = respRecognize[i].candidates;
                        if (respReg[Object.keys(respReg)[0]] > 0.6) {
                            io.to('user:' + req.user._id).emit('face:recognized', {index: i, suggest: Object.keys(respReg)[0]});
                        }
                    }
                    defer.resolve();
                },
                function (err) {
                }
            );
        });
        return defer.promise;
    };



    router.post("/upload/upload-new-image", Security.authorDetails,
        multer({
        dest: "./uploads/image-draft/"
    }).single("file"), function (req ,res) {
            var type = req.file.mimetype;
            if (type.indexOf('image/') != -1) {
                convertToPng(req.file.filename, req.user._id).then(function () {
                    if (req.body.filter == "true") {
                        Filter.filterImage(req.user._id + '_origin', req.user._id).then(function () {
                            //detectImage(req).then(function () {
                            //    //viewSubject(req).then(function () {
                            //        res.send({ok: true});
                            //    //});
                            //});
                            res.send({ok: true});
                        });
                    } else {
                        detectImage(req).then(function () {
                            //viewSubject(req).then(function () {
                            //    res.send({ok: true});
                            //});
                        });
                        res.send({ok: true});

                    }

                } , function (err) {
                    res.send({error: "Lỗi convert sang PNG"})
                })
            }

    })
};