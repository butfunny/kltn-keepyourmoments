var Like = require('./../../dao/like-dao');
var User = require('./../../dao/user-dao');
var Security = require('../security/security');
var Status = require('./../../dao/status-dao');
var LocationStatus = require('./../../dao/location-status');
var LocationAds = require('./../../dao/location-ads-dao');
var Location = require('./../../dao/location-dao');
var LocationFollow = require('./../../dao/location-follow');
var Follow = require('./../../dao/follow-dao');
var Notifications = require('./../../dao/noti-dao');
var Filter = require('../../filter/filter');
var fs = require('fs');
var NotificationSupport = require('./../notification/notification-support');
var DetectFace = require('./../../dao/detect-face-dao');
var DetectSupport = require('../../dectect-recognize-face/dectect-recognize-face');
var _ = require('lodash');
var $q = require('q');

module.exports = function (router, io) {


    var socketToUser = function (user_upload, status) {
        io.to('user:' + user_upload).emit('status:new', status);
        Follow.find({user_to_follow: user_upload}, function (err, users) {
            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                io.to('user:' + user.user_id).emit('status:new', status);
            }
        });
    };


    router.post("/new-status", Security.authorDetails, function (req, res) {

        var currentTime = new Date().getTime();

        var status = {
            user_id: req.user._id,
            text: req.body.text,
            image: currentTime + '.png',
            location: req.body.location_id
        };
        Filter.deleteImageNotInclude(req.body.image, req.user._id);
        fs.renameSync('./uploads/image-draft/' + req.user._id + '_' + req.body.image + '.png', './uploads/image-draft/' + currentTime + '.png');
        var promises = [];

        for (var i = 0; i < req.body.selection.length; i++) {
            var selected = req.body.selection[i];
            if (selected == "myProfile") {
                promises.push(Status.create(status, function (err, status) {
                    socketToUser(req.user._id, status);
                }))
            } else {
                var statusLocation = {
                    location_id: selected,
                    image: currentTime + '.png',
                    text: req.body.text,
                    isPostOfLocation: true,
                    created: new Date()
                };
                promises.push(LocationStatus.create(statusLocation, function () {
                    LocationFollow.find({location_id: selected}, function (err, follows) {
                        for (var j = 0; j < follows.length; j++) {
                            var follow = follows[j];
                            io.to('user:' + follow.user_follow).emit('status:new', statusLocation);
                        }
                    })
                }));
            }

        }

        $q.all(promises).then(function () {
            res.end();
        });

    });


    router.get("/status/prepare", Security.authorDetails, function (req, res) {
        Follow.find({user_id: req.user._id}, function (err, userFollow) {
            var user_follow = _.map(userFollow, 'user_to_follow');
            Status.find({$or: [{user_id: req.user._id}, {user_id: {$in: user_follow}}]}, null, {sort: {created: -1}}, function (err, status) {
                res.send(status);
            })
        });

    });

    router.post("/status/like/prepare", Security.authorDetails, function (req, res) {
        Like.find({status_id: {$in: req.body.status_ids}}, function (err, likes) {
            res.send(likes);
        })
    });

    router.post("/status/like", Security.authorDetails, function (req, res) {
        Like.findOne({user_id: req.user._id, status_id: req.body.status_id}, function (err, like) {
            if (like == null) {
                Like.create({user_id: req.user._id, status_id: req.body.status_id}, function (err, newLike) {
                    io.emit('like:new', newLike);
                    Status.findOne({_id: req.body.status_id}, function (err, s) {
                        if (s.user_id != req.user._id) {
                            var noti = {
                                owned: req.user._id,
                                type: "like",
                                params: {status_id: req.body.status_id}
                            };
                            NotificationSupport.sendNoti(io, noti, [s.user_id]);
                        }
                        res.send(newLike);
                    })

                })
            } else {
                res.end();
            }
        });
    });

    router.post("/status/unlike", Security.authorDetails, function (req, res) {
        Like.remove({status_id: req.body.status_id, user_id: req.user._id}, function () {
            io.emit('unlike:new', {status_id: req.body.status_id, user_id: req.user._id});
            res.end();
        })
    });

    router.delete("/status/delete/:sid", Security.authorDetails, function (req, res) {
        Status.findOne({_id: req.params.sid}, function (err, status) {
            //fs.unlinkSync('./uploads/image-draft/' + status.image);
            status.remove();
            io.to('user:' + req.user._id).emit('status:delete', req.params.sid);
            Follow.find({user_to_follow: req.user._id}, function (err, users) {
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    io.to('user:' + user.user_id).emit('status:delete', req.params.sid);
                }
                Like.remove({status_id: req.params.sid}, function () {
                });
                Notifications.remove({params: {status_id: req.params.sid}}, function () {
                });
                res.end();
            });
        })
    });

    router.put("/status/update", Security.authorDetails, function (req, res) {
        Status.findOneAndUpdate({_id: req.body._id}, {$set: {text: req.body.text}}, function (err) {
            io.to('user:' + req.user._id).emit('status:update', req.body);
            Follow.find({user_to_follow: req.user._id}, function (err, users) {
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    io.to('user:' + user.user_id).emit('status:update', req.body);
                }
                res.end();
            });
        })
    });

    router.get("/status/byUserID/:uid", function (req, res) {
        Status.find({user_id: req.params.uid}, null, {sort: {created: -1}}, function (err, status) {
            res.send(status);
        })
    });

    router.get("/status/like/:sid", function (req, res) {
        Like.find({status_id: req.params.sid}, function (err, likeUsers) {
            res.send(likeUsers);
        })
    });

    router.post("/status/get", function (req, res) {
        var statusIds = req.body;
        Status.find({_id: {$in: statusIds}}, function (err, status) {
            res.json(status);
        })
    });

    router.post("/status/ads", Security.authorDetails, function (req, res) {
        var toRad = function (number) {
            return number * Math.PI / 180;
        };

        var getDistance = function ($lat, $lng, lat, lng) {
            return ( 6371 * Math.acos(Math.cos(toRad($lat)) * Math.cos(toRad(lat)) * Math.cos(toRad(lng) - toRad($lng)) + Math.sin(toRad($lat)) * Math.sin(toRad(lat))));
        };


        Location.find(function (err, locations) {
            var allLocations = locations;
            var suggestLocation = [];
            for (var i = 0; i < allLocations.length; i++) {
                var location = allLocations[i];
                var distance = getDistance(parseFloat(req.body.lat), parseFloat(req.body.lng), parseFloat(location.pos.lat), parseFloat(location.pos.lng));
                if (distance < 20) {
                    suggestLocation.push(location);
                }
            }

            var locationIdsNearMe = _.map(suggestLocation, "_id");
            var finalLocationIds = [];
            LocationFollow.find({user_follow: req.user._id}, function (err, locations) {
                var locationIdsFollow = _.map(locations, "location_id");
                for (var i = 0; i < locationIdsNearMe.length; i++) {

                    if (locationIdsFollow.indexOf(locationIdsNearMe[i].toString()) == -1) {
                        finalLocationIds.push(locationIdsNearMe[i]);
                    }
                }
                LocationAds.find({location_id: {$in: finalLocationIds}}, function (err, ads) {
                    var statusIds = _.map(ads, "status_location_id");
                    LocationStatus.find({_id: {$in: statusIds}}, function (err, status) {
                        res.json(status);
                    })
                });
            })

        });
    })
};