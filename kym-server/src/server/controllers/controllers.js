var express = require("express");
var jwt = require('jsonwebtoken');
var router = express.Router();


module.exports = function (app, io) {
    io.on('connection', function (socket) {
        socket.emit('reconnect');
        socket.on('loggedIn', function (message) {
            jwt.verify(message.token, /* jwtSecret */  '23f34g1234yg1345yg13', function (err, decodedAuth) {
                if (err != null) {
                    return;
                }
                socket.join('user:' + decodedAuth._id);
            });
        });
        socket.on('loggedOut', function (message) {
            jwt.verify(message.token, /* jwtSecret */  '23f34g1234yg1345yg13', function (err, decodedAuth) {
                if (err != null) {
                    return;
                }
                socket.leave('user:' + decodedAuth._id);
            });
        });
    });



    app.use("/api", router);
    require("./security-controller")(router, io);
    require("./upload-controller")(router, io);
    require("./status-controller")(router, io);
    require("./follow-controller")(router, io);
    require("./comment-controller")(router, io);
    require("./search-controller")(router);
    require("./notification-controller")(router, io);
    require("./messsage-controller")(router, io);
    require("./location-controller")(router, io);
};