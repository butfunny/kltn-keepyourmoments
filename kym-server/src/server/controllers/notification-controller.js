var User = require('./../../dao/user-dao');
var Notification = require('./../../dao/noti-dao');
var UserNotification = require('./../../dao/user-noti-dao');
var Security = require('../security/security');
var _ = require('lodash');



module.exports = function (router, io) {

    router.get("/notification/me",Security.authorDetails, function (req, res) {
        UserNotification.find({user_id: req.user._id}, function (err, userNoti) {
            var notiIds = _.map(userNoti, "notification_id");
            Notification.find({_id: {$in: notiIds}}, function (err, notifications) {
                res.send(notifications);
            })
        });
    });

    router.post("/notification/me", Security.authorDetails, function (req ,res) {
        req.body.user_id = req.user._id;
        UserNotification.create(req.body, function () {
           res.end();
        })
    });

    router.put("/notification/seen", Security.authorDetails, function (req, res) {
        Notification.update({_id: {$in: req.body}}, { $set: { seen: true }},{multi: true}, function (err, noti) {
            res.end();
        })
    })

};