var Security = require('../security/security');
var _ = require('lodash');
var Status = require('./../../dao/status-dao');
var User = require('./../../dao/user-dao');
var Vi = require('../../vi/vi');


module.exports = function (router) {

    router.post("/search/people", function (req, res) {
        User.find({}, function (err, users) {
            res.json(_.filter(users, function(user) {
                return (user.name != null && Vi.removeMark(user.name.toLowerCase()).indexOf(req.body.keyword) > -1) || (user.username != null && Vi.removeMark(user.username.toLowerCase()).indexOf(req.body.keyword) > -1) || (user.email != null && Vi.removeMark(user.email.toLowerCase()).indexOf(req.body.keyword) > -1);
            }));
        })
    });

    router.post("/search/tag", function (req, res) {
        Status.find({}, function (err, status) {
            res.json(_.filter(status, function(s) {
                return (s.text != null && Vi.removeMark(s.text.toLowerCase()).indexOf('#' + req.body.keyword) > -1);
            }));
        })
    })


};