var Follow = require('./../../dao/follow-dao');
var User = require('./../../dao/user-dao');
var Security = require('../security/security');
var NotificationSupport = require('./../notification/notification-support');

var _ = require('lodash');



module.exports = function (router, io) {

    router.post("/follow", Security.authorDetails, function (req, res) {
        Follow.findOne({user_id: req.user._id, user_to_follow: req.body.user_id}, function (err, f) {
            if (f != null) {
                res.end();
            } else {
                Follow.create({user_id: req.user._id, user_to_follow: req.body.user_id}, function (err, follow) {
                    io.to('user:' + req.user._id).emit('follow:new', follow);
                    io.to('user:' + req.body.user_id).emit('follower:new', follow);
                    var noti = {
                        owned: req.user._id,
                        type: "follow",
                        params: {user_id: req.body.user_id}
                    };
                    NotificationSupport.sendNoti(io, noti, [req.body.user_id]);
                    res.end();
                })
            }
        })
    });

    router.delete("/unfollow/:id", Security.authorDetails, function (req, res) {
       Follow.findOneAndRemove({_id: req.params.id}, function (err, f) {
           io.to('user:' + req.user._id).emit('unfollow:new', {_id: req.params.id});
           io.to('user:' + f.user_to_follow).emit('follower:delete', req.params.id);
           res.end();
       })
    });


    router.get("/follow", Security.authorDetails, function (req ,res) {
        Follow.find({user_id: req.user._id}, function (err, followers) {
            res.send(followers);
        })
    });

    router.get("/follow/suggest", Security.authorDetails, function (req, res) {
        User.findOne({_id: req.user._id}, function (err, me) {
            User.find({facebook_id: {$in: me.friends}}, function (err, userSuggest) {
                res.json(userSuggest);
            })
        })
    });

    router.get("/follow/byUserID/:uid", function (req, res) {
        Follow.find({user_id: req.params.uid}, function (err, follows) {
            res.send(follows);
        })
    });

    router.get("/follower", Security.authorDetails, function (req, res) {
        Follow.find({user_to_follow: req.user._id}, function (err, follower) {
            res.send(follower);
        })
    });

    router.get("/follower/byUserID/:uid", function (req, res) {
        Follow.find({user_to_follow: req.params.uid}, function (err, follows) {
            res.send(follows);
        })
    });

};