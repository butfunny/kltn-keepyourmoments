var Location = require('./../../dao/location-dao');
var LocationFollow = require('./../../dao/location-follow');
var LocationRating = require('./../../dao/location-rating-dao');
var LocationStatus = require('./../../dao/location-status');
var LocationComment = require('./../../dao/location-comment-dao');
var LocationAds = require('./../../dao/location-ads-dao');
var LocationLike = require('./../../dao/location-like-dao');
var LocationRequest = require('./../../dao/location-request-dao');
var ChatSession = require('./../../dao/chat-session-dao');
var Message = require('./../../dao/message-dao');
var Status = require('./../../dao/status-dao');
var Security = require('../security/security');
var NotificationSupport = require('./../notification/notification-support');
var _ = require('lodash');
var multer = require('multer');
var $q = require('q');
var Vi = require('../../vi/vi');



module.exports = function (router, io) {

    var toRad = function (number) {
        return number * Math.PI / 180;
    };

    var getDistance = function ($lat, $lng, lat, lng) {
        return ( 6371 * Math.acos(Math.cos(toRad($lat)) * Math.cos(toRad(lat)) * Math.cos(toRad(lng) - toRad($lng)) + Math.sin(toRad($lat)) * Math.sin(toRad(lat))));
    };

    router.post("/location", Security.authorDetails, function (req, res) {
        LocationRequest.create({
            owned: req.user._id,
            name: req.body.name,
            pos: req.body.pos,
            info: req.body.info,
            status: "requesting"
        }, function (err, location) {
            //LocationFollow.create({
            //    user_follow: req.user._id,
            //    location_id: location._id
            //}, function () {
            //    res.json(location);
            //});
            res.end();
        });
    });

    router.get("/location", Security.authorDetails, function (req, res) {
        Location.find({owned: req.user._id}, function (err, locations) {
            res.json(locations);
        })
    });


    router.get("/location/checking", function (req, res) {
        var allLocationChecking = [];
        LocationRequest.find({}, function (err, locationsRequest) {
            Location.find(function (err, locations) {

                var allLocations = locations;
                for (var i = 0; i < locationsRequest.length; i++) {
                    var lr = locationsRequest[i];
                    var location = {
                        info: lr,
                        nearlyLocation: []
                    };
                    for (var j = 0; j < allLocations.length; j++) {
                        var distance = getDistance(parseFloat(lr.pos.lat), parseFloat(lr.pos.lng), parseFloat(allLocations[j].pos.lat), parseFloat(allLocations[j].pos.lng));
                        if (distance < 5) {
                            var infoNearlyLocation = {
                                location_id: allLocations[j]._id,
                                distance: distance
                            };
                            location.nearlyLocation.push(infoNearlyLocation);
                        }
                    }
                    allLocationChecking.push(location);
                }
                res.json(allLocationChecking);
            });
        })
    });

    router.post("/location/accept-location", function (req, res) {
        LocationRequest.remove({_id: req.body._id}).exec();
        delete req.body._id;
        Location.create(req.body, function (err, location) {
            var noti = {
                owned: null,
                type: "noti-location",
                params: {text: "Địa điểm " + req.body.name + " đã được duyệt"}
            };
            NotificationSupport.sendNoti(io, noti, [req.body.owned]);
            io.to('user:' + req.body.owned).emit('location:new', location);
            res.end();
        })
    });

    router.put("/location/decline-location", function (req, res) {
        LocationRequest.update({_id: req.body._id}, {status: "decline", reason: req.body.reason}, function (err) {
            var noti = {
                owned: null,
                type: "noti-location",
                params: {text: "Địa điểm " + req.body.name + " đã bị từ chối bởi lý do: " + req.body.reason}
            };
            NotificationSupport.sendNoti(io, noti, [req.body.owned]);
            res.end();
        })
    });

    router.post("/location/get", function (req, res) {
        var locationIds = req.body;
        Location.find({_id: {$in: locationIds}}, function (err, locations) {
            res.json(locations);
        })
    });

    router.post("/location/rating/", Security.authorDetails, function (req, res) {
        LocationRating.create({
            user_id: req.user._id,
            location_id: req.body.location_id,
            rate_number: req.body.rate_number
        }, function (err, rated) {
            res.send(rated);
        })
    });


    router.get("/location/rating/:lid", function (req, res) {
        LocationRating.find({location_id: req.params.lid}, function (err, ratings) {
            res.json(ratings);
        });
    });

    router.get("/location/follow/:lid", function (req, res) {
        LocationFollow.find({location_id: req.params.lid}, function (err, follower) {
            res.json(follower);
        })
    });

    router.post("/location/follow/:lid", Security.authorDetails, function (req, res) {
        LocationFollow.create({location_id: req.params.lid, user_follow: req.user._id}, function (err, follower) {
            res.json(follower);
        });
    });

    router.delete("/location/un-follow/:lid", Security.authorDetails, function (req, res) {
        LocationFollow.remove({location_id: req.params.lid, user_follow: req.user._id}, function (err, follower) {
            res.end();
        });
    });


    var getPostsUsingLocation = function (posts, locationId) {
        var defer = $q.defer();
        Status.find({location: locationId}, null, {sort: {created: -1}}, function (err, status) {
            for (var i = 0; i < status.length; i++) {
                var s = status[i];
                posts.users.push(s);
            }
            defer.resolve();
        });
        return defer.promise;
    };

    var getPostsOfLocation = function (posts, locationId) {
        var defer = $q.defer();
        LocationStatus.find({location_id: locationId}, null, {sort: {created: -1}}, function (err, status) {
            for (var i = 0; i < status.length; i++) {
                var s = status[i];
                posts.location.push(s);
            }
            defer.resolve();
        });
        return defer.promise;
    };

    router.get("/location/posts/:lid", function (req, res) {
        var posts = {
            location: [],
            users: []
        };
        $q.all([
            getPostsOfLocation(posts, req.params.lid),
            getPostsUsingLocation(posts, req.params.lid)
        ]).then(function () {
            res.json(posts);
        })
    });

    router.get("/location/prepare-status/user", Security.authorDetails, function (req, res) {
        LocationFollow.find({user_follow: req.user._id}, function (err, follows) {
            var location_ids = _.map(follows, "location_id");
            LocationStatus.find({location_id: {$in: location_ids}}, function (err, status) {
                res.json(status);
            })
        })
    });


    router.post("/location/like/prepare", function (req, res) {
        LocationLike.find({status_location_id: {$in: req.body.ids}}, function (err, likes) {
            res.json(likes);
        })
    });

    router.post("/location/comment/prepare", function (req, res) {
        LocationComment.find({status_location_id: {$in: req.body.ids}}, function (err, comments) {
            res.json(comments);
        });
    });

    router.post("/location/like-status/:lid", Security.authorDetails, function (req, res) {
        var likeInfo = {
            status_location_id: req.params.lid,
            user_id: req.user._id
        };
        LocationLike.create(likeInfo, function (err, like) {
            res.json(likeInfo);
        });
    });

    router.post("/location/like-status-manager/:lid", Security.authorDetails, function (req, res) {
        var likeInfo = {
            status_location_id: req.params.lid,
            user_id: req.body.location_id,
            admin: true
        };
        LocationLike.create(likeInfo, function (err, like) {
            res.json(likeInfo);
        });
    });


    router.delete("/location/unlike-status/:lid", Security.authorDetails, function (req, res) {
        LocationLike.remove({status_location_id: req.params.lid, user_id: req.user._id}, function () {
            res.end();
        })
    });

    router.delete("/location/unlike-status-manager/:lid", Security.authorDetails, function (req, res) {
        LocationLike.remove({status_location_id: req.params.lid, user_id: req.body.location_id}, function () {
            res.end();
        })
    });


    router.post("/location/comment-status", Security.authorDetails, function (req, res) {
        var comment = {
            user_id: req.user._id,
            status_location_id: req.body.location_status_id,
            text: req.body.text
        };
        LocationComment.create(comment, function (err, comment) {
            res.json(comment);
        });
    });

    router.post("/location/comment-status-manager", Security.authorDetails, function (req, res) {
        var comment = {
            user_id: req.body.location_id,
            status_location_id: req.body.location_status_id,
            text: req.body.text,
            admin: true
        };
        LocationComment.create(comment, function (err, comment) {
            res.json(comment);
        });
    });


    router.post("/location/change-with-avatar", Security.authorDetails, multer({
        dest: "./uploads/avatar/"
    }).single("file"), function (req, res) {
        var type = req.file.mimetype;
        if (type.indexOf('image/') != -1) {
            Location.findOneAndUpdate({_id: req.body._id}, {
                $set: {
                    avatar: req.file.filename,
                    info: req.body.info
                }
            }, function () {
                Location.findOne({_id: req.body._id}, function (err, location) {
                    res.send(location);
                })
            })
        } else {
            res.send({error: true});
        }
    });

    router.post("/location/change-info", Security.authorDetails, function (req, res) {
        Location.findOneAndUpdate({_id: req.body._id}, {$set: {info: req.body.info}}, function (err) {
            Location.findOne({_id: req.body._id}, function (err, location) {
                res.send(location);
            })
        })
    });


    router.post("/location/suggest", function (req, res) {

        Location.find(function (err, locations) {
            var allLocations = locations;
            var suggestLocation = [];
            for (var i = 0; i < allLocations.length; i++) {
                var location = allLocations[i];
                var distance = getDistance(parseFloat(req.body.lat), parseFloat(req.body.lng), parseFloat(location.pos.lat), parseFloat(location.pos.lng));
                if (distance < 5) {
                    suggestLocation.push({distance: distance, info: location});
                }
            }
            res.json(suggestLocation);

        });
    });

    var socketToUser = function (status_id) {
        LocationFollow.find({location_id: status_id}, function (err, followers) {
            for (var i = 0; i < followers.length; i++) {
                var follow = followers[i];
                io.to('user:' + follow.user_follow).emit('status:delete', status_id);
            }
        });
    };

    router.delete("/location/status/:sid", function (req, res) {
        LocationStatus.remove({_id: req.params.sid}, function () {
            socketToUser(req.params.sid);
            res.end();
        })
    });

    router.post("/location/ads", function (req, res) {
        LocationAds.findOne({location_id: req.body.location_id}, function (err, ads) {
            if (ads != null) {
                LocationAds.update({location_id: req.body.location_id}, {
                    status_location_id: req.body.status_location_id,
                    created: new Date()
                }, function (err, docs) {
                    res.end();
                })
            } else {
                LocationAds.create(req.body, function (err) {
                    res.end();
                })
            }
        });

    });

    router.get("/location/status-only/:sid", function (req, res) {
        LocationStatus.findOne({_id: req.params.sid}, function (err, status) {
            res.json(status);
        })
    });

    router.get("/location/search/:keyword", function (req, res) {
        Location.find({}, function (err, locations) {
            res.json(_.filter(locations, function(location) {
                return (location.name != null && Vi.removeMark(location.name.toLowerCase()).indexOf(req.params.keyword) > -1);
            }));
        })
    });

    router.get("/location/statistic", function (req, res) {
        Location.find({}, function (err, locations) {
            LocationFollow.find({}, function (err, locationFollow) {
                LocationStatus.find({}, function (err, locationStatus) {
                    LocationRating.find({}, function (err, ratings) {
                        res.json({locations: locations, locationFollow: locationFollow, locationStatus: locationStatus, ratings: ratings});
                    })
                })

            })
        })
    })
};