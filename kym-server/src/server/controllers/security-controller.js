var _ = require('lodash');
var jwt = require('jsonwebtoken');
var User = require('./../../dao/user-dao');
var Security = require('../security/security');
var crypto = require('crypto');
var multer = require('multer');
var fs = require('fs');
var NotificationSupport = require('./../notification/notification-support');


module.exports = function (router, io) {
    var createSecurityToken = function (userData) {
        return jwt.sign(userData, /* jwtSecret */ '23f34g1234yg1345yg13',{
            expiresIn: 1440
        });

    };



    router.post("/login-facebook", function (req, res) {
        User.findOne({facebook_id: req.body.id}, function (err, user) {
           if (user != null) {
               var token = createSecurityToken(user);
               res.json({token: token, user: user});
           } else {
               var friends = [];
               for (var i = 0; i < req.body.friends.length; i++) {
                   var friend = req.body.friends[i];
                   friends.push(friend.id);
               }
                var newUser = {
                    facebook_id : req.body.id,
                    name: req.body.name,
                    email: req.body.email,
                    friends: friends
                };
                User.create(newUser, function (err) {
                    User.findOne({facebook_id : req.body.id}, function (err, user) {
                        var token = createSecurityToken(user);
                        res.json({user: user, token: token});
                    });
                })
           }
        });
    });

    router.post("/security/check-token", function (req, res) {

        if (req.headers.authorization == null) {
            res.status(401).end();
            return;
        }

        var token = req.headers.authorization.replace(/^Bearer /, '');
        jwt.verify(token, /* jwtSecret */  '23f34g1234yg1345yg13', function (err, decodedAuth) {
            User.findOne({_id: decodedAuth._id}, function (err, user) {
                if (user == null) {
                    res.status(401).end();
                } else {
                    res.send({user: decodedAuth});
                }
            });
        });
    });

    router.post("/create-user-name",Security.authorDetails, function (req, res) {
        User.findOne({username: req.body.user_name}, function (err, userFind) {
            if (userFind == null) {
                User.findOneAndUpdate({_id: req.user._id}, {username: req.body.user_name, password: crypto.createHash('md5').update(req.body.password).digest("hex")}, function (err, user) {
                    var token = createSecurityToken(user);
                    var noti = {
                        owned: user._id,
                        type: "new-account",
                        params: {username: user.username}
                    };
                    User.find({facebook_id : {$in: user.friends}}, function (err, users) {
                        NotificationSupport.sendNoti(io, noti, _.map(users, "_id"));
                        res.json({user: user, token: token});
                    });
                })
            } else {
                res.send({error: true});
            }
        })
    });

    router.post("/login-password", function (req, res) {
        User.findOne({username: req.body.user_name, password: crypto.createHash('md5').update(req.body.password).digest("hex")}, function (err, user) {
            if (user != null) {
                var token = createSecurityToken(user);
                res.send({user: user, token: token});
            } else {
                res.send({error: true});
            }
        })
    });


    router.post("/user/change-with-avatar", Security.authorDetails, multer({
        dest: "./uploads/avatar/"
    }).single("file"), function (req, res) {
        var type = req.file.mimetype;
        if (type.indexOf('image/') != -1) {
            User.findOneAndUpdate({_id: req.user._id}, {$set: {avatar: true, avatar_url: req.user._id, name: req.body.name}}, function (err, oldUser) {
                fs.renameSync("./uploads/avatar/" + req.file.filename, "./uploads/avatar/" + req.user._id);
                User.findOne({_id: req.user._id}, function (err, user) {
                    var token = createSecurityToken(user);
                    io.to('user:' + req.user._id).emit('need-reload-image');
                    res.send({user: user, token: token});
                })
            })
        } else {
            res.send({error: true});
        }
    });

    router.post("/user/change-profile", Security.authorDetails, function (req, res) {
        User.findOneAndUpdate({_id: req.user._id}, {$set: {name: req.body.name}}, function (err) {
            User.findOne({_id: req.user._id}, function (err, user) {
                var token = createSecurityToken(user);
                res.send({user: user, token: token});
            })
        })
    });

    router.post("/users/get", function (req, res) {
        var userIds = req.body;
        User.find({_id: {$in: userIds}}, function (err, users) {
            res.json(users);
        })
    });


    router.get("/user/getById/:uid", Security.authorDetails, function (req ,res) {
        User.findOne({_id: req.params.uid}, function (err, user) {
            res.send(user);
        })
    })







};