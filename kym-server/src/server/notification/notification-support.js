var Notification = require('./../../dao/noti-dao');
var UserNotification = require('./../../dao/user-noti-dao');
module.exports = {
    sendNoti: function (io, noti, users) {

            Notification.findOne({type: noti.type, params: noti.params, owned: noti.owned}, function (err, n) {
                if (n == null) {
                    Notification.create(noti, function (err, newNoti) {
                        for (var i = 0; i < users.length; i++) {
                            var userID = users[i];
                            UserNotification.create({user_id: userID, notification_id: newNoti._id}, function () {

                            });
                            io.to('user:' + userID).emit('notification:new', newNoti);
                        }
                    });
                } else {
                    Notification.findOneAndUpdate({_id : n._id}, {$set: {created: new Date(), seen: false}}, function (err, oldNoti) {
                        Notification.findOne({_id: oldNoti._id}, function (err, newNoti) {
                            for (var i = 0; i < users.length; i++) {
                                var userID = users[i];
                                io.to('user:' + userID).emit('notification:new', newNoti);
                            }
                        })
                    });
                }
            })

    }
};
