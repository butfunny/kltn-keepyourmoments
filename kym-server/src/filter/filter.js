var Caman = require('caman').Caman;
var $q = require('q');
var fs = require('fs');

var pleasant = function (file, user_id) {
    var defer = $q.defer();

    Caman.Filter.register("pleasant", function() {
        this.colorize(60, 105, 218, 10);
        this.contrast(10);
        this.sunrise();
        this.hazyDays();
        this.render();
    });

    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.pleasant();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_pleasant.png");
            defer.resolve();
        });
    });

    return defer.promise;
};


var vintage = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.vintage();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_vintage.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var lomo = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.lomo();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_lomo.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var clarity = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.clarity();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_clarity.png");
            defer.resolve();
        });
    });
    return defer.promise;
};


var orangepeel = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.orangePeel();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_orangepeel.png");
            defer.resolve();
        });
    });
    return defer.promise;
};


var sunrise = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.sunrise();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_sunrise.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var crossprocess = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.crossProcess();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_crossprocess.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var hazyDays = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.hazyDays();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_hazyDays.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var jarques = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.jarques();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_jarques.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var pinhole = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.pinhole();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_pinhole.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var oldBoot = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.oldBoot();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_oldboot.png");
            defer.resolve();
        });
    });
    return defer.promise;
};

var glowingSun = function (file, user_id) {
    var defer = $q.defer();
    Caman("./uploads/image-draft/" + file + ".png", function () {
        this.glowingSun();
        this.render(function () {
            this.save("./uploads/image-draft/" + user_id + "_glowingSun.png");
            defer.resolve();
        });
    });
    return defer.promise;
};


module.exports = {

    filterImage : function (file, user_id) {
        return $q.all([
            pleasant(file, user_id),
            vintage(file, user_id),
            lomo(file, user_id),
            clarity(file, user_id),
            orangepeel(file, user_id),
            sunrise(file, user_id),
            crossprocess(file, user_id),
            hazyDays(file, user_id),
            jarques(file, user_id),
            pinhole(file, user_id),
            oldBoot(file, user_id),
            glowingSun(file, user_id)
        ]);
    },
    deleteImageNotInclude: function (typeName, user_id) {
        var types = ["pleasant",
            "vintage",
            "lomo",
            "clarity",
            "orangepeel",
            "sunrise",
            "crossprocess",
            "hazyDays",
            "jarques",
            "pinhole",
            "oldboot",
            "glowingSun",
            "origin"
        ];

        for (var i = 0; i < types.length; i++) {
            var type = types[i];
            if (type != typeName) {
                if (fs.existsSync('./uploads/image-draft/' + user_id + '_' + type + '.png')) {
                    fs.unlinkSync('./uploads/image-draft/' + user_id + '_' + type + '.png');
                }
            }
        }
    }
};
