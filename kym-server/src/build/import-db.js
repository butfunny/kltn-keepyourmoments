var gulp = require('gulp');
var $q = require('q');
var fs = require('fs');
var mockImportFiles = [
    "--collection comment --db kym /Workon/kltn-keepyourmoments/kym/comment.bson",
    "--collection detect_face --db kym /Workon/kltn-keepyourmoments/kym/detect_face.bson",
    "--collection like --db kym /Workon/kltn-keepyourmoments/kym/like.bson",
    "--collection message --db kym /Workon/kltn-keepyourmoments/kym/message.bson",
    "--collection notification --db kym /Workon/kltn-keepyourmoments/kym/notification.bson",
    "--collection status --db kym /Workon/kltn-keepyourmoments/kym/status.bson",
    "--collection user --db kym /Workon/kltn-keepyourmoments/kym/user.bson",
    "--collection user_notification --db kym /Workon/kltn-keepyourmoments/kym/user_notification.bson",
    "--collection chat_session --db kym /Workon/kltn-keepyourmoments/kym/chat_session.bson"
];


gulp.task('import-db', function () {
    var pormises = [];
    var cmdRestoreDB = "--collection filename --db kym /Workon/kltn-keepyourmoments/kym/filename.bson";
    var terminal = require('child_process').spawn('bash');

    function execCollection (collection) {
        var defer = $q.defer();
        var terminal = require('child_process').spawn('bash');
        terminal.stdin.write('/usr/local/bin/mongorestore ' + collection);
        terminal.stdin.end();
        defer.resolve();
        return defer.promise;
    }

    function getBsonFile (files) {
        var findedFiles = [];
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var typeOfFile = file.substring(file.length - 4, file.length);
            if (typeOfFile == 'bson') {
                findedFiles.push(file.substring(0, file.length - 5));
            }
        }
        return findedFiles;
    }

    fs.readdir('../kym', function (err, files) { var bsonFile = getBsonFile(files);

        for (var i = 0; i < bsonFile.length; i++) {

            var file = bsonFile[i];
            var collection = cmdRestoreDB.replace(/filename/gi, file);
            pormises.push(execCollection(collection));
        }

        $q.all(pormises).then(function () {
            console.log("Done");
        })
    });









});