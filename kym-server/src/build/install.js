var gulp = require('gulp');
var fs = require('fs');
gulp.task('install', function () {
    if (!fs.existsSync('./uploads')){
        fs.mkdir('./uploads', function () {
            if (!fs.existsSync('./uploads/image-draft')) {
                fs.mkdirSync('./uploads/image-draft');
            }
            if (!fs.existsSync('./uploads/avatar')) {
                fs.mkdirSync('./uploads/avatar');
            }
        });
    }
});

