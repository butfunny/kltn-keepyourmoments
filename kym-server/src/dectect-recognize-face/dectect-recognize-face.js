var $q = require('q');
var fs = require('fs');
var unirest = require('unirest');

module.exports = {
    detect: function (image) {
        var defer = $q.defer();
        unirest.post("https://animetrics.p.mashape.com/detect?api_key=d9b9909ce276747643c9d5763e003105")
            .header("X-Mashape-Key", "5hGClkYFjymshttKIDIdhegp2Tbop1GzPgBjsnki3zOZDS8j0I")
            .attach("image", fs.createReadStream(image))
            .field("selector", "FULL")
            .end(function (result) {
                defer.resolve(result.body);
            });
        return defer.promise;
    },
    enrollFaceWithEye: function (image_id, leftEyeCenterX, leftEyeCenterY,rightEyeCenterX, rightEyeCenterY, subject_id) {
        var defer = $q.defer();
        unirest.get("https://animetrics.p.mashape.com/enroll?api_key=d9b9909ce276747643c9d5763e003105&gallery_id=KeepYourMoments&image_id="+ image_id +"&leftEyeCenterX="+ leftEyeCenterX + "&leftEyeCenterY="+ leftEyeCenterY +"&rightEyeCenterX="+rightEyeCenterX+"&rightEyeCenterY="+rightEyeCenterY+"&subject_id="+ subject_id)
            .header("X-Mashape-Key", "5hGClkYFjymshttKIDIdhegp2Tbop1GzPgBjsnki3zOZDS8j0I")
            .header("Accept", "application/json")
            .end(function (result) {
                //"images": [
                //    {
                //        "time": 4.86605,
                //        "transaction": {
                //            "status": "Complete",
                //            "face_id": "19a7e956a9d0ecd2e1e4e99ca6e9ba32",
                //            "image_id": "3a351933dfab67d66e97f2754e691277",
                //            "subject_id": "thu",
                //            "gallery_id": "KeepYourMoments"
                //        }
                //    }
                //]
                defer.resolve(result.body.images);
            });
        return defer.promise;
    },
    recognize: function (image_id, leftEyeCenterX, leftEyeCenterY, rightEyeCenterX, rightEyeCenterY) {
        var defer = $q.defer();
        unirest.get("https://animetrics.p.mashape.com/recognize?api_key=d9b9909ce276747643c9d5763e003105&gallery_id=KeepYourMoments&image_id=" + image_id + "&leftEyeCenterX=" +leftEyeCenterX +"&leftEyeCenterY=" + leftEyeCenterY + "&rightEyeCenterX=" + rightEyeCenterX +"&rightEyeCenterY=" + rightEyeCenterY)
            .header("X-Mashape-Key", "5hGClkYFjymshttKIDIdhegp2Tbop1GzPgBjsnki3zOZDS8j0I")
            .header("Accept", "application/json")
            .end(function (result) {
                defer.resolve(result.body.images[0]);
                //"images": [
                //    {
                //        "time": 5.2702,
                //        "transaction": {
                //            "status": "Complete",
                //            "face_id": "7601eba027c9232004cecb9e2f94ebff",
                //            "image_id": "39e4071842ec4a9555a3ddfd880a0818",
                //            "gallery_id": "KeepYourMoments"
                //        },
                //        "candidates": {
                //            "cuongdota": "0.6969193816185",
                //            "Cuong": "0.667023003101349",
                //            "Nguyen": "0.572729289531708",
                //            "thu": "0.495493084192276"
                //        }
                //    }
                //]
            });
        return defer.promise;
    },viewSubject: function (subject_id) {
        var defer = $q.defer();
        unirest.get("https://animetrics.p.mashape.com/view_subject?api_key=d9b9909ce276747643c9d5763e003105&subject_id=" + subject_id)
            .header("X-Mashape-Key", "5hGClkYFjymshttKIDIdhegp2Tbop1GzPgBjsnki3zOZDS8j0I")
            .end(function (result) {
                if(result.body.errors || result.body.face_ids.length < 5) {
                    defer.reject();
                } else {
                    defer.resolve();
                }
            });
        return defer.promise;
    }
};
