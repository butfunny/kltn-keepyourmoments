"use strict";

(function () {

    angular.module("kym.update-information", [
        "ui.router"
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state("update-information", {
                    url: "/update-information",
                    templateUrl: "angular/update-information/update-information.html",
                    controller: "update-information.ctrl"
                })
            ;
        }])

        .controller("update-information.ctrl", function($scope, User, Security, $state, $ionicPopup) {
            $scope.User = User;

            $scope.user = {};

            $scope.createUserName = function () {

              if ($scope.user.user_name.length < 6) {
                $ionicPopup.alert({
                  title: 'Lỗi',
                  template: "Tên tài khoản phải có ít nhất 6 kí tự"
                });
                return;
              }

              if ($scope.user.password.length < 6) {
                $ionicPopup.alert({
                  title: 'Lỗi',
                  template: 'Password phải có ít nhất 6 kí tự'
                });
                return;
              }

              if ($scope.user.password != $scope.user.re_password) {
                $ionicPopup.alert({
                  title: 'Lỗi',
                  template: "Sai pass nhập lại"
                });
                return;
              }

              Security.createUserName($scope.user).then(function () {
                $state.go("login-facebook");
              }, function () {
                $ionicPopup.alert({
                  title: 'Lỗi',
                  template: "User name đã có người sử dụng!"
                });
              })
            };

            $scope.logout = function () {
              Security.logout();
            }
        })

    ;

})();
