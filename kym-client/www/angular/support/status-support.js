"use strict";

(function () {

    angular.module("kym.status-support", [
    ])
        .factory("$status", function(User, $user, $locationsCache, Api, $q, SocketIO, Security, $cordovaGeolocation) {
            var deferReady = $q.defer();
            var status = [];

            var prepareLocationPost = function (locationPosts) {
              var defer = $q.defer();
              var statusLocationIds = _.map(locationPosts, "_id");
              Api.post("/api/location/like/prepare", {ids: statusLocationIds}).then(function (respLike) {
                for (var i = 0; i < locationPosts.length; i++) {
                  var s = locationPosts[i];
                  s.likes = [];
                  for (var j = 0; j < respLike.data.length; j++) {
                    var like = respLike.data[j];
                    if (like.status_location_id == s._id) {
                      s.likes.push(like);
                    }
                  }
                }
                Api.post("/api/location/comment/prepare", {ids: statusLocationIds}).then(function (respComments) {
                  for (var i = 0; i < locationPosts.length; i++) {
                    var s = locationPosts[i];
                    s.comments = [];
                    for (var j = 0; j < respComments.data.length; j++) {
                      var comment = respComments.data[j];
                      if (comment.status_location_id == s._id) {
                        s.comments.unshift(comment);
                      }
                    }
                  }
                  defer.resolve(locationPosts);
                })
              });

              return defer.promise;
            };





        Security.onLogin(
              function () {
                Api.get("/api/status/prepare").then(function (resp) {
                  var status1 = resp.data;
                  var statusIds = _.map(status1, '_id');
                  var userIds = _.map(status1, 'user_id');
                  var locationIds = _.map(status1, 'location');
                  $user.prepareUsers(userIds);
                  $locationsCache.prepareLocations(locationIds);
                  Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
                    for (var i = 0; i < status1.length; i++) {
                      var s = status1[i];
                      s.likes = [];
                      for (var j = 0; j < respLike.data.length; j++) {
                        var like = respLike.data[j];
                        if (like.status_id == s._id) {
                          s.likes.push(like);
                        }
                      }
                    }
                    Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
                      for (var i = 0; i < status1.length; i++) {
                        var s = status1[i];
                        s.comments = [];
                        for (var j = 0; j < respComments.data.length; j++) {
                          var comment = respComments.data[j];
                          if (comment.status_id == s._id) {
                            s.comments.unshift(comment);
                          }
                        }
                      }

                      Api.get("/api/location/prepare-status/user").then(function (resp) {
                        prepareLocationPost(resp.data).then(function (locationPosts) {
                          for (var i = 0; i < locationPosts.length; i++) {
                            var s = locationPosts[i];
                            s.isPostOfLocation = true;
                            status1.push(s);
                          }
                          angular.copy(status1, status);
                          deferReady.resolve();
                        })
                      });


                    })
                  });
                });
              },
              function () {
                angular.copy([], status);
                deferReady = $q.defer();
              }
            );



            SocketIO.onAlways('status:new', function (s) {
              status.unshift(s);
            });

            SocketIO.onAlways('status:delete', function (s) {
              _.remove(status, function (status) {
                return status._id == s;
              })
            });

            SocketIO.onAlways('status:update', function (info) {
              var s = _.find(status, function (s) {
                return s._id == info._id
              });
              s.text = info.text;
            });

            SocketIO.onAlways('like:new', function (like) {
              var s = _.find(status, function (s) {
                return s._id == like.status_id
              });

              if (s) {
                if (!s.likes) s.likes = [];
                s.likes.push(like);
              }
            });

            SocketIO.onAlways('unlike:new', function (info) {
              var s = _.find(status, function (s) {
                return s._id == info.status_id
              });

              if (s) {
                _.remove(s.likes, function(l) {
                  return l.user_id == info.user_id;
                });
              }
            });


            SocketIO.onAlways('comment:new', function (comment) {
              var s = _.find(status, function (s) {
                return s._id == comment.status_id
              });

              if (s) {
                if (!s.comments) s.comments = [];
                s.comments.unshift(comment);
              }
            });

            SocketIO.onAlways('comment:delete', function (comment) {
              var s = _.find(status, function (s) {
                return s._id == comment.status_id
              });

              if (s) {
                _.remove(s.comments, function (c) {
                  return c._id == comment._id;
                })
              }
            });

            return {
                ready: function () {
                  return deferReady.promise;
                },
                getAllStatus: function () {
                  return status;
                },
                addStatus: function (s) {
                  status.push(s);
                },
                insertStatus: function (s, index) {
                  status.splice(index, 0, s);
                },
                getLike: function (status_id) {
                  var s = _.find(status, function (s) {
                    return s._id == status_id;
                  });
                  return s.likes;
                },
                reload: function () {
                  var defer = $q.defer();
                  var newStatus = [];
                  Api.get("/api/status/prepare").then(function (resp) {
                    newStatus = resp.data;
                    var statusIds = _.map(newStatus, '_id');
                    var userIds = _.map(newStatus, 'user_id');
                    $user.prepareUsers(userIds);
                    Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
                      for (var i = 0; i < newStatus.length; i++) {
                        var s = newStatus[i];
                        s.likes = [];
                        for (var j = 0; j < respLike.data.length; j++) {
                          var like = respLike.data[j];
                          if (like.status_id == s._id) {
                            s.likes.push(like);
                          }
                        }
                      }
                      Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
                        for (var i = 0; i < newStatus.length; i++) {
                          var s = newStatus[i];
                          s.comments = [];
                          for (var j = 0; j < respComments.data.length; j++) {
                            var comment = respComments.data[j];
                            if (comment.status_id == s._id) {
                              s.comments.unshift(comment);
                            }
                          }
                        }

                        Api.get("/api/location/prepare-status/user").then(function (resp) {
                          prepareLocationPost(resp.data).then(function (locationPosts) {
                            for (var i = 0; i < locationPosts.length; i++) {
                              var s = locationPosts[i];
                              s.isPostOfLocation = true;
                              newStatus.push(s);
                            }
                            angular.copy(newStatus, status);
                            defer.resolve();
                          })
                        });


                      })
                    });
                  });
                  return defer.promise;

                },
                isMyStatus: function (status_id) {
                  var myStatus = _.filter(status, function (s) {
                    return s.user_id == User._id
                  });
                  return _.findIndex(myStatus, function (s) {
                    return s._id == status_id;
                  })
                }
            };
        })

    ;

})();
