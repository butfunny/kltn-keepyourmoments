"use strict";

(function () {

    angular.module('kym.status-searched-support', [
    ])

       .factory("$statusSearched", function(Security, Api, $q, $user) {
          var statusSearched = [];
          Security.onLogin(function () {}, function () {
            angular.copy([],statusSearched);
          });
           return {
             addStatusSearched: function (status) {
               var defer = $q.defer();
               angular.copy(status, statusSearched);
               var statusIds = _.map(statusSearched, '_id');
               var userIds = _.map(statusSearched, 'user_id');
               $user.prepareUsers(userIds);
               Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
                 for (var i = 0; i < statusSearched.length; i++) {
                   var s = statusSearched[i];
                   s.likes = [];
                   for (var j = 0; j < respLike.data.length; j++) {
                     var like = respLike.data[j];
                     if (like.status_id == s._id) {
                       s.likes.push(like);
                     }
                   }
                 }
                 Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
                   for (var i = 0; i < statusSearched.length; i++) {
                     var s = statusSearched[i];
                     s.comments = [];
                     for (var j = 0; j < respComments.data.length; j++) {
                       var comment = respComments.data[j];
                       if (comment.status_id == s._id) {
                         s.comments.unshift(comment);
                       }
                     }
                   }
                   defer.resolve();
                 })
               });
               return defer.promise;
             },
             getStatusSearched: function () {
               return statusSearched;
             }
           };
       })

    ;

})();
