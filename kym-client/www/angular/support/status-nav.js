"use strict";

(function () {

    angular.module("kym.status-nav", [
    ])
      .directive("statusNav", function() {
        return {
          restrict: "A",
          controller: function($scope, $user, $status, Api, User, statusApi, $ionicLoading, $ionicActionSheet, $ionicPopup, $ionicModal, commentApi, $state, $locationsCache) {
            $scope.$user = $user;
            $scope.Api = Api;
            $scope.$locationsCache = $locationsCache;

            $scope.isLike = function (status) {
              return _.findIndex(status.likes, function (l) {
                return l.user_id == User._id;
              })
            };
            if ($scope.s.faces && typeof $scope.s.faces == "string") $scope.s.faces = JSON.parse($scope.s.faces);


            $scope.goToProfile = function (userID) {
              if ($scope.modalComment) $scope.modalComment.hide();
              if (User._id == userID) $state.go("main-app.account");
              else $state.go("main-app.user-view", {userID: userID});
            };

            $scope.like = function (status) {
              $ionicLoading.show({
                template: '<i class="ion-ios-heart"></i>'
              });
              setTimeout(function () {
                $ionicLoading.hide();
              }, 1000);
              statusApi.likeStatus(status._id);
            };

            $scope.likeButton = function (status) {
              if ($scope.isLike(status) == -1) {
                statusApi.likeStatus(status._id);
              } else {
                statusApi.unlikeStatus(status._id);
              }
            };

            $scope.viewImage = function(imageSrc) {
              $ionicModal.fromTemplateUrl('angular/support/image-view/image-view.html', {
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modalImageView = modal;
                $scope.imageSrc = imageSrc;
                $scope.viewImage.viewTag = false;
                $scope.modalImageView.show();
              });
            };

            $scope.toggleViewTag = function () {
              $scope.viewImage.viewTag = !$scope.viewImage.viewTag;
            };


            $scope.isChanged = function () {
              return !angular.equals($scope.view, $scope.s);
            };


            $scope.more = function (status) {
              $ionicActionSheet.show({
                buttons: [
                  { text: 'Sửa <i class="icon ion-edit"></i>' }
                ],
                destructiveText: 'Xóa',
                cancelText: 'Hủy',
                buttonClicked: function(index) {
                  if (index == 0) {
                    $ionicModal.fromTemplateUrl("angular/main-app/modal/edit-status-modal.html", {
                      scope: $scope,
                      animation: "slide-in-up"
                    }).then(function(modal) {
                      $scope.modal = modal;
                      $scope.modal.show();
                      $scope.view = angular.copy($scope.s);
                    });
                  }
                  return true;
                },
                destructiveButtonClicked: function() {
                  $ionicPopup.confirm({
                    title: 'Xóa ảnh?'
                  }).then(function (resp) {
                    if(resp) {
                      statusApi.deleteStatus(status._id);
                    }
                  });
                  return true;
                }
              });
            };

            $scope.updateStaus = function () {
              statusApi.updateStatus($scope.view);
              $scope.modal.hide();
            };

            $scope.comment = function (s) {
              $ionicModal.fromTemplateUrl("angular/main-app/modal/comment-modal.html", {
                scope: $scope,
                animation: "slide-in-up"
              }).then(function(modal) {
                $scope.modalComment = modal;
                $scope.modalComment.show();
              });
            };

            $scope.viewComments = {
              text: ''
            };

            $scope.deleteComment = function (comment) {
              $ionicPopup.confirm({
                title: 'Xóa bình luận?'
              }).then(function (resp) {
                if(resp) {
                  commentApi.remove(comment._id);
                }
              });
            };

            $scope.commentStatus = function (status) {
              $scope.viewComments.status_id = status._id;
              commentApi.comment($scope.viewComments).then(function () {
                $scope.viewComments.text = '';
              });
            };

            $scope.$status = $status;
          }
        };
      })

      .directive("statusNavWithoutSocket", function() {
          return {
              restrict: "A",
              controller: function($scope, $user, $locationsCache, $status, Api, $stateParams, User, statusApi, $ionicLoading, $ionicActionSheet, $ionicPopup, $ionicModal, commentApi, $state) {
                $scope.$locationsCache = $locationsCache;
                $scope.Api = Api;
                $scope.$user = $user;
                $scope.goToProfile = function (userID) {
                  if ($scope.modalComment) $scope.modalComment.hide();
                  if ($stateParams.userID == userID){
                    $state.go("main-app.user-view", {userID: userID });
                    return;
                  }
                  if (User._id == userID) {
                    $state.go("main-app.account");
                  } else {
                    $state.go("main-app.user-view", {userID: userID });
                  }
                };

                $scope.isLike = function (status) {
                  return _.findIndex(status.likes, function (l) {
                    return l.user_id == User._id;
                  })
                };

                if ($scope.s.faces && typeof $scope.s.faces == "string") $scope.s.faces = JSON.parse($scope.s.faces);

                $scope.viewImage = function(imageSrc) {
                  $ionicModal.fromTemplateUrl('angular/support/image-view/image-view.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                  }).then(function(modal) {
                    $scope.modalImageView = modal;
                    $scope.imageSrc = imageSrc;
                    $scope.viewImage.viewTag = false;
                    $scope.modalImageView.show();
                  });
                };

                $scope.toggleViewTag = function () {
                  $scope.viewImage.viewTag = !$scope.viewImage.viewTag;
                };

                $scope.like = function (status) {
                  $ionicLoading.show({
                    template: '<i class="ion-ios-heart"></i>'
                  });
                  setTimeout(function () {
                    $ionicLoading.hide();
                  }, 1000);
                  statusApi.likeStatus(status._id).then(function (resp) {
                    status.likes.push(resp.data);
                  });
                };

                $scope.likeButton = function (status) {
                  if ($scope.isLike(status) == -1) {
                    statusApi.likeStatus(status._id).then(function (resp) {
                      status.likes.push(resp.data);
                    });
                  } else {
                    statusApi.unlikeStatus(status._id).then(function (resp) {
                      _.remove(status.likes, function (l) {
                        return l.user_id == User._id;
                      })
                    });
                  }
                };

                $scope.comment = function (s) {
                  $ionicModal.fromTemplateUrl("angular/main-app/modal/comment-modal.html", {
                    scope: $scope,
                    animation: "slide-in-up"
                  }).then(function(modal) {
                    $scope.modalComment = modal;
                    $scope.modalComment.show();
                    $scope.s = s;
                  });
                };

                $scope.viewComments = {
                  text: ''
                };


                $scope.commentStatus = function (status) {
                  $scope.viewComments.status_id = status._id;
                  commentApi.comment($scope.viewComments).then(function (resp) {
                    $scope.viewComments.text = '';
                    status.comments.unshift(resp.data);
                  });
                };
              }
          };
      })


      .directive("locationStatusNav", function() {
          return {
              restrict: "A",
              controller: function($scope, $ionicModal, locationApi, User, $user, Api, $state, $locationsCache) {
                $scope.$locationsCache = $locationsCache;
                $scope.isLike = function (status) {
                  return _.findIndex(status.likes, function (l) {
                    return l.user_id == User._id;
                  })
                };

                $scope.comment = function (s) {
                  $ionicModal.fromTemplateUrl("angular/main-app/modal/comment-modal.html", {
                    scope: $scope,
                    animation: "slide-in-up"
                  }).then(function(modal) {
                    $scope.modalComment = modal;
                    $scope.modalComment.show();
                  });
                };


                $scope.likeButton = function (status) {
                  if ($scope.isLike(status) == -1) {
                    locationApi.likeStatus(status._id).then(function (resp) {
                      status.likes.push(resp.data);
                    });
                  } else {
                    locationApi.unlikeStatus(status._id).then(function (resp) {
                      _.remove(status.likes, function (l) {
                        return l.user_id == User._id;
                      })
                    });
                  }
                };

                $scope.viewComments = {};
                $scope.$user = $user;
                $scope.Api = Api;

                $scope.commentStatus = function (status) {
                  $scope.viewComments.location_status_id = status._id;
                  locationApi.commentStatus($scope.viewComments).then(function (resp) {
                    status.comments.unshift(resp.data);
                    $scope.viewComments.text = '';
                  });
                };

                $scope.goToProfile = function (userID) {
                  if ($scope.modalComment) $scope.modalComment.hide();
                  if (User._id == userID) $state.go("main-app.account");
                  else $state.go("main-app.user-view", {userID: userID});
                };
              }
          };
      })

      .directive("locationStatusNavManager", function() {
          return {
              restrict: "A",
              controller: function($scope, $ionicModal, locationApi, User, $user, Api, $state, $locationsCache) {
                $scope.isLike = function (status) {
                  return _.findIndex(status.likes, function (l) {
                    return l.user_id == $scope.location._id;
                  })
                };

                $scope.$locationsCache = $locationsCache;

                $scope.comment = function (s) {
                  $ionicModal.fromTemplateUrl("angular/main-app/modal/comment-modal.html", {
                    scope: $scope,
                    animation: "slide-in-up"
                  }).then(function(modal) {
                    $scope.modalComment = modal;
                    $scope.modalComment.show();
                  });
                };


                $scope.likeButton = function (status) {
                  if ($scope.isLike(status) == -1) {
                    locationApi.likeStatusWithManager(status._id, $scope.location._id).then(function (resp) {
                      status.likes.push(resp.data);
                    });
                  } else {
                    locationApi.unlikeStatusWithManager(status._id, $scope.location._id).then(function (resp) {
                      _.remove(status.likes, function (l) {
                        return l.user_id == $scope.location._id;
                      })
                    });
                  }
                };

                $scope.viewComments = {};
                $scope.$user = $user;
                $scope.Api = Api;

                $scope.commentStatus = function (status) {
                  $scope.viewComments.location_status_id = status._id;
                  $scope.viewComments.location_id = $scope.location._id;
                  locationApi.commentStatusWithManager($scope.viewComments).then(function (resp) {
                    status.comments.unshift(resp.data);
                    $scope.viewComments.text = '';
                  });
                };

                $scope.goToProfile = function (userID) {
                  if ($scope.modalComment) $scope.modalComment.hide();
                  if (User._id == userID) $state.go("main-app.account");
                  else $state.go("main-app.user-view", {userID: userID});
                };

              }
          };
      })
    ;

})();
