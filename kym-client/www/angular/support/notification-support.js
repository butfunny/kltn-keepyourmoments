"use strict";

(function () {

    angular.module("kym.notification-support", [
    ])
        .factory("$notifications", function(Security, Api, SocketIO, $q) {
            var notifications = [];
            var deferReady = $q.defer();
            Security.onLogin(function () {
              Api.get("/api/notification/me").then(function(resp) {
                angular.copy(resp.data, notifications);
                deferReady.resolve();
              });
            }, function () {
              angular.copy([], notifications);
            });


            SocketIO.onAlways('notification:new', function (noti) {
              var n = _.find(notifications, function (notiFind) {
                return notiFind._id == noti._id
              });
              if (n) {
                angular.copy(noti, n);
              } else {
                notifications.unshift(noti);
              }
            });
            return {
              ready: function () {
                return deferReady.promise;
              },
              getNoti: function () {
                return notifications;
              },
              getCountNoti: function () {
                return _.filter(notifications, function (n) {
                  return n.seen == false;
                }).length;
              },
              seenAllNoti: function () {
                var notiID = _.map(notifications, "_id");
                Api.put("/api/notification/seen", notiID).then(function () {
                  for (var i = 0; i < notifications.length; i++) {
                    var noti = notifications[i];
                    noti.seen = true;
                  }
                });
              },
              reload: function () {
                var defer = $q.defer();
                Api.get("/api/notification/me").then(function(resp) {
                  angular.copy(resp.data, notifications);
                  defer.resolve();
                });
                return defer.promise;
              }
            };
        })

    ;

})();
