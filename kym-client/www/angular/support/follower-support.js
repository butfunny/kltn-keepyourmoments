"use strict";

(function () {

    angular.module("kym.follower-support", [
    ])
        .factory("$follower", function(Security, Api, $user, SocketIO, $q) {
            var deferReady = $q.defer();
            var follower = [];

            Security.onLogin(
              function () {
                Api.get("/api/follower").then(function (resp) {
                  follower = resp.data;
                  var userIds = _.map(follower, 'user_id');
                  $user.prepareUsers(userIds);
                  deferReady.resolve();
                });
              },
              function () {
                angular.copy([], follower);
                deferReady = $q.defer();
              }
            );

            SocketIO.onAlways("follower:new", function (follow) {
              follower.push(follow);
            });

            SocketIO.onAlways("follower:delete", function (id) {
              _.remove(follower, function (f) {
                return f._id == id;
              })
            });

            return {
              ready: function () {
                return deferReady.promise;
              },
              getMyFollwer: function () {
                return follower;
              }
            };
        })
    ;

})();
