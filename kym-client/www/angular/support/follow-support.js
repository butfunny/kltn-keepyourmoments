"use strict";

(function () {

  angular.module("kym.follow-support", [
  ])
    .factory("$follow", function(User, $user, Api, $q, SocketIO, Security, $status) {
      var deferReady = $q.defer();
      var follow = [];

      Security.onLogin(
        function () {
          Api.get("/api/follow").then(function (resp) {
            follow = resp.data;
            var userIds = _.map(follow, 'user_to_follow');
            $user.prepareUsers(userIds);
            deferReady.resolve();
          });
        },
        function () {
          angular.copy([], follow);
          deferReady = $q.defer();
        }
      );

      SocketIO.onAlways('follow:new', function (s) {
        $status.reload();
        follow.unshift(s);
      });


      SocketIO.onAlways('unfollow:new', function (info) {
        _.remove(follow, function(f) {
          return f._id == info._id;
        });
        $status.reload();
      });

      return {
        ready: function () {
          return deferReady.promise;
        },
        getMyFollower: function () {
          return follow;
        },
        isFollowed: function (user_id) {
          return _.find(follow, function (f) {
            return f.user_to_follow == user_id;
          })
        },
        getFollow: function (user_to_follow) {
          return _.find(follow, function (f) {
            return f.user_to_follow == user_to_follow;
          })
        }
      };
    })

  ;

})();
