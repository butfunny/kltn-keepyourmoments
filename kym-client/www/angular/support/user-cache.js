"use strict";

(function () {

  angular.module("kym.user-cache", [])
    .factory("$user", function (cacheSupport, Api) {
      var cacher = cacheSupport.newInstance(Api.getServerUrl() + "/api/users/get");
      return {
        prepareUsers: cacher.prepareIds,
        getUser: cacher.get
      };
    })

    .factory("$statusCache", function (cacheSupport, Api) {
      var cacher = cacheSupport.newInstance(Api.getServerUrl() + "/api/status/get");
      return {
        prepareStatus: cacher.prepareIds,
        getStatus: cacher.get
      };
    })

    .factory("$locationsCache", function(cacheSupport, Api) {
      var cacher = cacheSupport.newInstance(Api.getServerUrl() + "/api/location/get");
      return {
        prepareLocations: cacher.prepareIds,
        getLocation: cacher.get
      };
    })

    .run(function ($faceDetect) {

    })

    .factory("$faceDetect", function (SocketIO) {
      var facesDetected = [];

      SocketIO.onAlways('face:detected', function (faces) {
        facesDetected = angular.copy(faces);
      });

      SocketIO.onAlways('face:recognized', function (info) {
        facesDetected[info.index].faceName = info.suggest;
      });

      return {
        getFaces: function () {
          return facesDetected;
        }
      };
    })

  ;

})();
