"use strict";

(function () {

  angular.module("kym.location-support", [
  ])
    .factory("$locations", function(Security, Api, $user, SocketIO, $q) {
      var deferReady = $q.defer();
      var locations = [];

      Security.onLogin(
        function () {
          Api.get("/api/location").then(function (resp) {
            locations = resp.data;
            deferReady.resolve();
          });
        },
        function () {
          angular.copy([], locations);
          deferReady = $q.defer();
        }
      );

      SocketIO.onAlways("location:new", function (location) {
        locations.push(location);
      });

      return {
        ready: function () {
          return deferReady.promise;
        },
        getLocations: function () {
          return locations;
        },
        addNewLoaction: function (location) {
          locations.push(location);
        },
        getLocation: function (id) {
          return _.find(locations, function (l) {
            return l._id == id;
          })
        }
      };
    })
  ;

})();
