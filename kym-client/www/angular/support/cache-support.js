"use strict";

(function () {

    angular.module("kym.cache-support", [
    ])
      .factory("cacheSupport", function($q, $http) {
        var resolvedDefer = $q.defer();
        resolvedDefer.resolve();

        return {
          newInstance: function(location) {

            var cache = {};

            var cacher;
            return cacher = {
              prepareIds: function(ids) {

                var promises = [];
                var reqIds = [];

                for (var i = 0; i < ids.length; i++) {
                  var id = ids[i];

                  var defer;
                  if (cache[id] != null) {
                    defer = cache[id].$defer;
                    if (defer) {
                      //console.log("Already requesting: " + id);
                      //console.log("Already requesting: " + JSON.stringify(cache[id]));
                      promises.push(defer.promise);
                    }
                  } else {
                    defer = $q.defer();
                    cache[id] = {
                      $defer: defer
                    };
                    reqIds.push(id);
                  }
                }

                if (!_.isEmpty(reqIds)) {
                  //console.log(JSON.stringify(reqIds) + " sent");
                  promises.push($http.post(location, reqIds).then(function(resp) {
                    var objects = resp.data;

                    for (var i = 0; i < reqIds.length; i++) {
                      var id = reqIds[i];

                      var defer = cache[id].$defer;

                      var obj = _.find(objects, function(o) { return o._id == id; });
                      if (obj == null) {
                        // Not found this id
                        cache[id].$defer = null;
                        cache[id].$not_found = true;
                      } else {
                        angular.copy(obj, cache[id]);
                      }

                      //console.log("Resolved " + id);
                      if (defer) {
                        defer.resolve();
                      }
                    }
                    //console.log(JSON.stringify(cache));
                  }));
                }
                //console.log("Wait for " + promises.length + " promises");
                return $q.all(promises);
              },
              get: function(id) {
                if (id == null) return null;

                if (!cache.hasOwnProperty(id)) {
                  cacher.prepareIds([id]);
                }
                return cache[id];
              },
              update: function(list) {
                if (list == null) return;

                for (var i = 0; i < list.length; i++) {
                  var obj = list[i];

                  var inCache = cache[obj._id];
                  if (inCache == null) {
                    cache[obj._id] = obj;
                  } else if (inCache.$defer) {
                    var defer = inCache.$defer;

                    angular.copy(obj, inCache);
                    defer.resolve(inCache);
                  } else if (!angular.equals(obj, inCache)) {
                    angular.copy(obj, inCache);
                  }
                }
              }
            };
          }
        };
      })
    ;

})();
