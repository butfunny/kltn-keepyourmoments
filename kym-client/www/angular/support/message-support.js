"use strict";

(function () {

    angular.module('kym.message-support', [
    ])
        .factory("$chatSessions", function(Api, $q, Security, SocketIO, User) {
          var chatSessions = [];
          var deferReady = $q.defer();
          Security.onLogin(function () {
            Api.get("/api//message/session/me").then(function (respSessions) {
              var sessions = respSessions.data;
              var sessionIds = _.map(sessions, "_id");
              Api.post("/api/message/message-by-session-ids", sessionIds).then(function (respMessage) {
                for (var i = 0; i < sessions.length; i++) {
                  var session = sessions[i];
                  session.messages = [];
                  for (var j = 0; j < respMessage.data.length; j++) {
                    var message = respMessage.data[j];
                    if (message.session_id == session._id) {
                      session.messages.push(message);
                    }
                  }
                }
                angular.copy(sessions, chatSessions);
                deferReady.resolve();
              })
            })
          }, function () {
            angular.copy([], chatSessions);
            deferReady = $q.defer();
          });
          SocketIO.onAlways("session:new", function (messSession) {
            chatSessions.push(messSession);
          });

          SocketIO.onAlways("message:new", function (message) {
            var session = _.find(chatSessions, function (c) {
              return c._id == message.session_id;
            });
            if (!session.messages) {
              session.messages = [];
            }
            session.messages.push(message);
          });

          SocketIO.onAlways("message:seen", function (newSession) {
            var session = _.find(chatSessions, function (s) {
              return s._id == newSession._id;
            });
            session.user1_seen = newSession.user1_seen;
            session.user2_seen = newSession.user2_seen;
          });

            return {
              ready: function () {
                return deferReady.promise;
              },
              getSessions: function () {
                return chatSessions;
              },
              getSessionById: function (sid) {
                return _.find(chatSessions, function (c) {
                  return c._id == sid;
                });
              },
              countNotiMessage: function () {
                var count = 0;
                for (var i = 0; i < chatSessions.length; i++) {
                  var session = chatSessions[i];
                  var index = _.findIndex(session.users_owned, function (u) {
                    return u == User._id;
                  });
                  var dateSeen;
                  if (index == 0) {
                    dateSeen = new Date(session.user1_seen).getTime();
                  } else {
                    dateSeen = new Date(session.user2_seen).getTime()
                  }
                  if (session.messages) {
                    var messages = session.messages;
                    for (var j = messages.length - 1; j >= 0; j--) {
                      var mes = messages[j];
                      if (mes.user_id != User._id) {
                        var timeLastMessage = new Date(mes.created).getTime();
                        if (dateSeen < timeLastMessage) {
                          count++;
                          break;
                        }
                      }
                    }
                  }
                }
                return count;
              },
              isSeenSession: function (session) {
                var index = _.findIndex(session.users_owned, function (u) {
                  return u == User._id;
                });
                var dateSeen;
                if (index == 0) {
                  dateSeen = new Date(session.user1_seen).getTime();
                } else {
                  dateSeen = new Date(session.user2_seen).getTime()
                }
                if (session.messages) {
                  var messages = session.messages;
                  for (var j = messages.length - 1; j >= 0; j--) {
                    var mes = messages[j];
                    if (mes.user_id != User._id) {
                      var timeLastMessage = new Date(mes.created).getTime();
                      if (dateSeen < timeLastMessage) {
                        return false;
                      }
                    }
                  }
                }

                return true;
              }
            };
        })
    ;

})();
