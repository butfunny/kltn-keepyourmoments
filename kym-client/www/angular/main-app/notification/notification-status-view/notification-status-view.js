"use strict";

(function () {

  angular.module("kym.main-app.notification-status-view", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.notification-status-view", {
          url: "/notification-status-view?:type/:status_id",
          views: {
            'notification': {
              templateUrl: "angular/main-app/notification/notification-status-view/notification-status-view.html",
              controller: "notification-status-view.ctrl"
            }
          }
        })
      ;
    }])

    .controller("notification-status-view.ctrl", function($scope, $statusCache, Api, $user, $stateParams, $state, User, $ionicLoading, statusApi, $ionicModal, commentApi) {
      $scope.Api = Api;
      $scope.$user = $user;
      Api.post("/api/status/like/prepare", {status_ids: $stateParams.status_id}).then(function (respLike) {
        $scope.s = $statusCache.getStatus($stateParams.status_id);
        $scope.s.likes = respLike.data;
        Api.post("/api/comment/prepare", {status_ids: $stateParams.status_id}).then(function (respComments) {
          $scope.s.comments = [];
          for (var i = 0; i < respComments.data.length; i++) {
            var comment = respComments.data[i];
            $scope.s.comments.push(comment);
          }
          $scope.ready = true;
          $ionicLoading.hide();
          if ($stateParams.type == 'comment') {
            $scope.comment($scope.s);
          }
        });
        if ($scope.s.faces && typeof $scope.s.faces == "string") $scope.s.faces = JSON.parse($scope.s.faces);

      });

      $scope.viewImage = function(imageSrc) {
        $ionicModal.fromTemplateUrl('angular/support/image-view/image-view.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modalImageView = modal;
          $scope.imageSrc = imageSrc;
          $scope.viewImage.viewTag = false;
          $scope.modalImageView.show();
        });
      };


      $scope.toggleViewTag = function () {
        $scope.viewImage.viewTag = !$scope.viewImage.viewTag;
      };


      $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
      });

      $scope.goToProfile = function (userID) {
        if ($scope.modalComment) $scope.modalComment.hide();
        if (User._id == userID) {
          $state.go("main-app.account");
        } else {
          $state.go("main-app.user-view", {userID: userID });
        }
      };

      $scope.isLike = function (status) {
        return _.findIndex(status.likes, function (l) {
          return l.user_id == User._id;
        })
      };

      $scope.like = function (status) {
        $ionicLoading.show({
          template: '<i class="ion-ios-heart"></i>'
        });
        setTimeout(function () {
          $ionicLoading.hide();
        }, 1000);
        statusApi.likeStatus(status._id).then(function (resp) {
          status.likes.push(resp.data);
        });
      };

      $scope.likeButton = function (status) {
        if ($scope.isLike(status) == -1) {
          statusApi.likeStatus(status._id).then(function (resp) {
            status.likes.push(resp.data);
          });
        } else {
          statusApi.unlikeStatus(status._id).then(function (resp) {
            _.remove(status.likes, function (l) {
              return l.user_id == User._id;
            })
          });
        }
      };
      $ionicModal.fromTemplateUrl("angular/main-app/modal/comment-modal.html", {
        scope: $scope,
        animation: "slide-in-up"
      }).then(function(modal) {
        $scope.modalComment = modal;
      });

      $scope.comment = function (s) {
        $scope.modalComment.show();
      };
      $scope.viewComments = {
        text: ''
      };


      $scope.commentStatus = function (status) {
        $scope.viewComments.status_id = status._id;
        commentApi.comment($scope.viewComments).then(function (resp) {
          $scope.viewComments.text = '';
          status.comments.unshift(resp.data);
        });
      };




    })

  ;

})();
