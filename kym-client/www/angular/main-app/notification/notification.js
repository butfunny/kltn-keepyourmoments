"use strict";

(function () {

  angular.module("kym.main-app.notification", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.notification", {
          url: "/notification",
          views: {
            'notification': {
              templateUrl: "angular/main-app/notification/notification.html",
              controller: "notification.ctrl"
            }
          }
        })
      ;
    }])

    .controller("notification.ctrl", function ($scope, User, $notifications, $statusCache, Api, $user, $status, $follow, followApi, $ionicActionSheet) {
      $scope.notifications = $notifications.getNoti();
      $scope.doRefresh = function () {
        $notifications.reload().then(function () {
          $scope.$broadcast('scroll.refreshComplete');
        })
      };

      $scope.$statusCache = $statusCache;
      $scope.Api = Api;
      $scope.$user = $user;
      $scope.$status = $status;
      $scope.$follow = $follow;

      $scope.follow = function (user_id) {
        followApi.follow(user_id);
      };

      $scope.unFollow = function (user_id) {
        $ionicActionSheet.show({
          destructiveText: 'Bỏ theo dõi',
          cancelText: 'Hủy',
          destructiveButtonClicked: function () {
            followApi.unFollow($follow.getFollow(user_id)._id);
            return true;
          }
        });
      };
    });

  ;

})();
