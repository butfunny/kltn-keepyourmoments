"use strict";

(function () {

    angular.module("kym.main-app.message", [
        "ui.router"
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state("main-app.message", {
                    url: "/message",
                    views: {
                      'message': {
                        templateUrl: "angular/main-app/message/message.html",
                        controller: "message.ctrl"
                      }
                    }
                })
            ;
        }])

        .controller("message.ctrl", function($scope, $chatSessions, User, $user, $ionicModal, searchApi, Api, $ionicScrollDelegate, messageApi, $state, $timeout) {
          $chatSessions.ready().then(function () {
            $scope.sessions = $chatSessions.getSessions();
          });
          $scope.getUserInSession = function (session) {
            var users_owned = session.users_owned;
            for (var i = 0; i < users_owned.length; i++) {
              if (users_owned[i] != User._id) return users_owned[i];
            }
          };
          $scope.sortByTime = function (session) {
            if (session.messages) return -new Date(session.messages[session.messages.length - 1].created).getTime();
          };

          $scope.view = {};
          $scope.Api = Api;

          $scope.$user = $user;
          $ionicModal.fromTemplateUrl("angular/main-app/message/new-message-modal.html", {
            scope: $scope,
            animation: "slide-in-up"
          }).then(function(modal) {
            $scope.modal = modal;
          });
          $scope.newMessage = function () {
            $scope.modal.show();
          };
          $scope.$watch("view.keyword", function(value) {
            if (!value){
              $scope.view.userSearched = [];
              return;
            }
            searchApi.searchPeople({keyword: value.toLowerCase()}).then(function (resp) {
              if ($scope.view.usersSend) {
                _.remove(resp.data, function (u) {
                  return u._id == $scope.view.usersSend._id;
                });
              }
              _.remove(resp.data, function (u) {
                return u._id == User._id;
              });
              $scope.view.userSearched = resp.data;
            });
          });
          $scope.addUser = function (user) {
            $scope.view.userSearched = [];
            $scope.view.keyword = undefined;
            $scope.view.usersSend = user;
          };

          $scope.search = {};




          $scope.sendMessage = function () {
            var message = {
              to_user: $scope.view.usersSend._id,
              text: $scope.view.messageText
            };
            messageApi.sendMessage(message).then(function (resp) {
              $scope.view.userSearched = [];
              $scope.view.keyword = undefined;
              $scope.view.usersSend = undefined;
              $scope.modal.hide();
              $timeout(function () {
                $state.go("main-app.message-view", {session_id: resp.data.session_id});
              }, 150);
            })
          };

          $scope.isSeenSession = function (session) {
            return $chatSessions.isSeenSession(session);
          };

          $scope.goToSession = function (session) {
            messageApi.seenSession(session._id);
            $state.go("main-app.message-view", {session_id: session._id});
          }



      })

    ;

})();
