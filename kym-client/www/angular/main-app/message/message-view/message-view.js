"use strict";

(function () {

    angular.module('kym.main-app.message-view', [
        'ui.router'
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state('main-app.message-view', {
                    url: '/message-view?:session_id',
                    views: {
                      'message': {
                        templateUrl: "angular/main-app/message/message-view/message-view.html",
                        controller: "message-view.ctrl"
                      }
                    }
                })
            ;
        }])

        .controller("message-view.ctrl", function($scope, $stateParams, $chatSessions, User, $user, Api, $ionicScrollDelegate, $timeout, messageApi, $state) {
          $scope.session = $chatSessions.getSessionById($stateParams.session_id);
          $scope.getTitleMessage = function () {
            var index = $scope.session.users_owned.indexOf(User._id);
            if (index == 0) {
              return $user.getUser($scope.session.users_owned[index + 1]).username;
            } else {
              return $user.getUser($scope.session.users_owned[index - 1]).username;
            }
          };

          $scope.User = User;
          $scope.$user = $user;
          $scope.Api = Api;

          $scope.$watch("session.messages.length", function(value) {
            messageApi.seenSession($scope.session._id);
            $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom();
          });

          $scope.seenSession = function () {
            messageApi.seenSession($scope.session._id);
          };

          $scope.goToProfile = function (userID) {
            if (User._id == userID) $state.go("main-app.account");
            else $state.go("main-app.user-view", {userID: userID});
          };

          $timeout(function () {
            $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom();
          }, 150);

          $scope.sendMessage = function () {
            var index = $scope.session.users_owned.indexOf(User._id);
            var to_user;
            if (index == 0) {
              to_user = $scope.session.users_owned[index + 1];
            } else {
              to_user = $scope.session.users_owned[index - 1];
            }
            var message = {
              to_user: to_user,
              text: $scope.messageText
            };
            messageApi.sendMessage(message).then(function (resp) {
              $scope.messageText = undefined;
            })
          };

          $scope.displayAvatar = function (index) {
            var messages = $scope.session.messages;
            if (index == 0) return true;
            else {
              return messages[index - 1].user_id != messages[index].user_id;
            }

          }

      })

    ;

})();
