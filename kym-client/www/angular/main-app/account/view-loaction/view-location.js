"use strict";

(function () {

  angular.module("kym.main-app.view-location", [
    "ui.router",
    "ionic.rating"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view-location", {
          url: "/view-location/:location_id",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/view-loaction/view-location.html",
              controller: "main-app.view-location.ctrl"
            }
          }

        })
      ;
    }])

    .controller("main-app.view-location.ctrl", function ($scope, $stateParams, $locations, locationApi, $q, $ionicLoading, User, $ionicPopup, Api, $ionicModal, $listUsersView, $state, $ionicActionSheet) {
      $scope.location = $locations.getLocation($stateParams.location_id);
      $scope.Api = Api;

      var lat = parseFloat($scope.location.pos.lat);
      var lng = parseFloat($scope.location.pos.lng);

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: {
          lat: lat,
          lng: lng
        }
      });

      var marker = new google.maps.Marker({
        position: {
          lat: lat,
          lng: lng
        },
        map: map,
        title: $scope.location.name
      });


      $scope.isOpenRateDetail = false;
      $scope.openRateDetail = function () {
        $scope.isOpenRateDetail = !$scope.isOpenRateDetail;
      };


      var getRatingInfo = function () {
        var defer = $q.defer();
        locationApi.getRating($scope.location._id).then(function (resp) {
          $scope.location.rating = resp.data;
          defer.resolve();
        });
        return defer.promise;
      };

      var getFollowInfo = function () {
        var defer = $q.defer();
        locationApi.getFollow($scope.location._id).then(function (resp) {
          $scope.follower = resp.data;
          defer.resolve();
        });
        return defer.promise;
      };


      var prepareLocationPost = function (locationPosts) {
        var defer = $q.defer();
        $scope.locationPosts = locationPosts;
        var statusLocationIds = _.map($scope.locationPosts, "_id");
        Api.post("/api/location/like/prepare", {ids: statusLocationIds}).then(function (respLike) {
          for (var i = 0; i < $scope.locationPosts.length; i++) {
            var s = $scope.locationPosts[i];
            s.likes = [];
            for (var j = 0; j < respLike.data.length; j++) {
              var like = respLike.data[j];
              if (like.status_location_id == s._id) {
                s.likes.push(like);
              }
            }
          }
          Api.post("/api/location/comment/prepare", {ids: statusLocationIds}).then(function (respComments) {
            for (var i = 0; i < $scope.locationPosts.length; i++) {
              var s = $scope.locationPosts[i];
              s.comments = [];
              for (var j = 0; j < respComments.data.length; j++) {
                var comment = respComments.data[j];
                if (comment.status_location_id == s._id) {
                  s.comments.unshift(comment);
                }
              }
            }
            defer.resolve();
          })
        });

        return defer.promise;
      };

      var prepareUserPost = function (userPosts) {
        var defer = $q.defer();
        $scope.userPosts = userPosts;
        var statusIds = _.map($scope.userPosts, '_id');
        Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
          for (var i = 0; i < $scope.userPosts.length; i++) {
            var s = $scope.userPosts[i];
            s.likes = [];
            for (var j = 0; j < respLike.data.length; j++) {
              var like = respLike.data[j];
              if (like.status_id == s._id) {
                s.likes.push(like);
              }
            }
          }
          Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
            for (var i = 0; i < $scope.userPosts.length; i++) {
              var s = $scope.userPosts[i];
              s.comments = [];
              for (var j = 0; j < respComments.data.length; j++) {
                var comment = respComments.data[j];
                if (comment.status_id == s._id) {
                  s.comments.unshift(comment);
                }
              }
            }
            defer.resolve();
          })
        });
        return defer.promise;

      };


      var getPosts = function () {
        var defer = $q.defer();
        locationApi.getPosts($scope.location._id).then(function (resp) {
          $q.all([
            prepareLocationPost(resp.data.location),
            prepareUserPost(resp.data.users)
          ]).then(function () {
            defer.resolve();
          })

        });
        return defer.promise;
      };


      $scope.getAvgRating = function () {
        if (!$scope.location.rating) return 0;
        var total = 0;

        for (var i = 0; i < $scope.location.rating.length; i++) {
          var rate = $scope.location.rating[i];
          total += parseInt(rate.rate_number);
          if (rate.user_id == User._id) {
            $scope.location.canRate = false;
          }
        }

        if ($scope.location.rating.length == 0) {
          return 0;
        } else {
          return (total / $scope.location.rating.length).toFixed(1);
        }
      };

      $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
      });
      $q.all([
        getRatingInfo(),
        getFollowInfo(),
        getPosts()
      ]).then(function () {
        $ionicLoading.hide();
      });

      $scope.view = {};


      $scope.options = ["Bài đăng của địa điểm", "Bài đăng của người dùng"];
      $scope.selected = $scope.options[0];

      $scope.viewFollower = function () {
        $listUsersView.setList($scope.follower);
        $state.go("main-app.view-follow-location-manager", {type: 'follow'});
      };

      $scope.viewLiker = function (status) {
        $listUsersView.setList(status.likes);
        $state.go("main-app.view-follow-location-manager", {type: 'like'});
      };


      $scope.editProfile = function () {
        $ionicModal.fromTemplateUrl("angular/main-app/account/view-loaction/minified-location.html", {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          $scope.view = angular.copy($scope.location);
          $scope.modalEdit = modal;
          $scope.modalEdit.show();
        })
      };


      $scope.isChanged = function () {
        return !angular.equals($scope.view, $scope.location);
      };

      $scope.declineChange = function () {
        if ($scope.isChanged()) {
          $ionicPopup.confirm({
            title: 'Có sự thay đổi',
            template: 'Nếu bạn xác nhận dữ liệu vừa rồi bạn nhập sẽ mất. Bạn có chắc chắn?'
          }).then(function (res) {
            if (res) {
              $scope.modalEdit.hide();
              $scope.view = angular.copy($scope.location);
            }
          })
        } else {
          $scope.modalEdit.hide();
        }
      };

      $scope.acceptChange = function () {
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
        });
        if ($scope.view.avatarClone) {
          Api.upload("/api/location/change-with-avatar", $scope.view.avatarClone, {info: $scope.view.info, _id: $scope.location._id}).then(function (resp) {
            $ionicLoading.hide();
            $scope.location.avatar = resp.data.avatar;
            $scope.location.info = resp.data.info;
            $scope.modalEdit.hide();
          });
        } else {
          Api.post("/api/location/change-info", {_id: $scope.location._id ,info: $scope.view.info}).then(function (resp) {
            $ionicLoading.hide();
            $scope.location.info = resp.data.info;
            $scope.modalEdit.hide();
          })
        }
      };


      $scope.optionsSort = ["Thời gian", "Lượng thích"];
      $scope.view.selectedSort = $scope.optionsSort[0];

      $scope.$watch("view.selectedSort", function(selectedSort) {
        if ($scope.userPosts) {
          if (selectedSort == "Thời gian") {
            $scope.userPosts.sort(function (a, b) {
              return a.created > b.created ? 1 : -1;
            });
          } else {
            $scope.userPosts.sort(function (a, b) {
              return b.likes.length - a.likes.length;
            });
          }
        }
      });


      $scope.more = function (status) {
        $ionicActionSheet.show({
          buttons: [
            { text: 'Quảng cáo bài viết <i class="icon ion-arrow-graph-up-right"></i>' }
          ],
          destructiveText: 'Xóa',
          cancelText: 'Hủy',
          buttonClicked: function(index) {
            if (index == 0) {
              locationApi.adsStatus({location_id: $scope.location._id, status_location_id: status._id}).then(function () {
               $ionicPopup.alert({
                  title: 'Thông báo',
                  template: 'Đăng quảng cáo bài viết của bạn thành công.'
                });
              })
            }
            return true;
          },
          destructiveButtonClicked: function() {
            $ionicPopup.confirm({
              title: 'Xóa ảnh?'
            }).then(function (resp) {
              if(resp) {
                locationApi.deleteStatus(status._id).then(function () {
                  _.remove($scope.locationPosts, function (s) {
                    return s._id == status._id
                  })
                });
              }
            });
            return true;
          }
        });
      }




    })




  ;

})();
