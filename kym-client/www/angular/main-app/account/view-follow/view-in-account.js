"use strict";

(function () {

  angular.module("kym.main-app.view", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view", {
          url: "/view?:type/:status_id",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/view-follow/view-in-account.html",
              controller: "view.ctrl"
            }
          }
        })
      ;
    }])

    .controller("view.ctrl", function($scope, $stateParams, $status, Api, $user, User, $follow, followApi, $ionicActionSheet, $state, $follower) {
      if ($stateParams.status_id && $stateParams.type == 'like') {
        $scope.listUserView = $status.getLike($stateParams.status_id);
      }

      if ($stateParams.type == 'follow') {
        $scope.listUserView = $follow.getMyFollower();
      }

      if ($stateParams.type == 'follower') {
        $scope.listUserView = $follower.getMyFollwer();
      }

      $scope.Api = Api;
      $scope.$user = $user;
      $scope.User = User;
      $scope.$follow = $follow;

      $scope.follow = function (user_id) {
        followApi.follow(user_id);
      };

      $scope.unFollow = function (user_id) {
        $ionicActionSheet.show({
          destructiveText: 'Bỏ theo dõi',
          cancelText: 'Hủy',
          destructiveButtonClicked: function() {
            followApi.unFollow($follow.getFollow(user_id)._id);
            return true;
          }
        });
      };

      $scope.goToProfile = function (userID) {
        if (User._id == userID) $state.go("main-app.account");
        else $state.go("main-app.user-view", {userID: userID});
      };

      $scope.getAvatarUrl = function (user) {
        var user_id = user.user_id;
        if ($stateParams.type == 'follow'){
           user_id = user.user_to_follow;
        }
        return $user.getUser(user_id).avatar ?
          (Api.getServerUrl() + '/uploads/avatar/' + $user.getUser(user_id).avatar_url) :
          'https://graph.facebook.com/ ' + $user.getUser(user_id).facebook_id + '/picture?type=large';
      };

      $scope.getUserID = function (user) {
        if ($stateParams.type == 'follow'){
          return user.user_to_follow;
        }
        return user.user_id
      }


    })

  ;

})();
