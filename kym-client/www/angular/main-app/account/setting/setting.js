"use strict";

(function () {

  angular.module("kym.main-app.setting", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.setting", {
          url: "/setting",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/setting/setting.html",
              controller: "main-app.setting.ctrl"
            }
          }

        })
      ;
    }])

    .controller("main-app.setting.ctrl", function ($scope, Security, $ionicHistory, $locations, User, locationApi) {
      $scope.logout = function () {
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        Security.logout();
      };

      $scope.User = User;
      $scope.$locations = $locations;

      locationApi.getCheckLocation().then(function (resp) {
        $scope.locationCheck = resp.data;
      });

      $scope.checkLocationRequest = function () {
        if (!$scope.locationCheck) return 0;
        var count = 0;
        for (var i = 0; i < $scope.locationCheck.length; i++) {
          var location = $scope.locationCheck[i];
          if (location.info.status == "requesting") count++;
        }
        return count;
      }

    })




  ;

})();
