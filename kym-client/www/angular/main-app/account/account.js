"use strict";

(function () {

  angular.module("kym.main-app.account", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.account", {
          url: "/account",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/account.html",
              controller: "account.ctrl"
            }
          }
        })
      ;
    }])

    .controller("account.ctrl", function ($scope, User, Api, statusApi, $ionicModal, $ionicPopup, Security, $ionicLoading, $status, $follow, $follower, SocketIO) {
      $scope.accountView = {};

      $status.ready().then(function () {
        $scope.status = $status.getAllStatus();
        $scope.statusLength = _.filter($scope.status, function (s) {
          return s.user_id == User._id
        }).length;
      });

      $scope.$follower = $follower;

      $follow.ready().then(function () {
        $scope.$follow = $follow;
      });

      $scope.User = User;
      $scope.Api = Api;
      $scope.view = angular.copy($scope.User);

      $scope.minifiedProfile = function () {
        $ionicModal.fromTemplateUrl("angular/main-app/account/minified-profile.html", {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          $scope.view = angular.copy(User);
          $scope.modal = modal;
          $scope.modal.show();
        });
      };

      $scope.isChanged = function () {
        return !angular.equals($scope.view, $scope.User);
      };

      $scope.declineChange = function () {
        if ($scope.isChanged()) {
          $ionicPopup.confirm({
            title: 'Có sự thay đổi',
            template: 'Nếu bạn xác nhận dữ liệu vừa rồi bạn nhập sẽ mất. Bạn có chắc chắn?'
          }).then(function (res) {
            if (res) {
              $scope.modal.hide();
              $scope.view = angular.copy($scope.User);
            } else {
              return;
            }
          })
        } else {
          $scope.modal.hide();
        }
      };


      $scope.$watch("User", function (value) {
        if (value.isLogged) {
          var d = new Date();
          $scope.avatar = (User.avatar) ? (Api.getServerUrl() + '/uploads/avatar/' + User._id + '?' + d.getTime()) : 'https://graph.facebook.com/' + User.facebook_id + '/picture?type=large';
        }
      });

      SocketIO.onAlways("need-reload-image", function () {
        var d = new Date();
        $scope.avatar = (User.avatar) ? (Api.getServerUrl() + '/uploads/avatar/' + User._id + '?' + d.getTime()) : 'https://graph.facebook.com/' + User.facebook_id + '/picture?type=large';
      });

      SocketIO.onAlways("need-reload-image", function () {
        $scope.accountView.reloadImage();
      });

      $scope.acceptChange = function () {
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
        });
        if ($scope.view.avatarClone) {
          Api.upload("/api/user/change-with-avatar", $scope.view.avatarClone, {name: $scope.view.name}).then(function (resp) {
            $ionicLoading.hide();
            Security.reloadToken(resp.data.user, resp.data.token);
            $scope.view = angular.copy($scope.User);
            $scope.modal.hide();
          });
        } else {
          Api.post("/api/user/change-profile", {name: $scope.view.name}).then(function (resp) {
            $ionicLoading.hide();
            Security.reloadToken(resp.data.user, resp.data.token);
            $scope.view = angular.copy($scope.User);
            $scope.modal.hide();
          })
        }
      }




    })

  ;

})();
