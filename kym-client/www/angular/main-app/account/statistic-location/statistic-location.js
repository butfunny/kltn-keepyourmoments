"use strict";

(function () {

  angular.module("kym.main-app.statistic-location", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.statistic-location", {
          url: "/statistic-location",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/statistic-location/statistic-location.html",
              controller: "statistic-location.ctrl"
            }
          }

        })
      ;
    }])

    .controller("statistic-location.ctrl", function ($scope, locationApi) {
      locationApi.getStatisticLocation().then(function (resp) {
        $scope.locations = resp.data.locations;
        for (var i = 0; i < $scope.locations.length; i++) {
          var location = $scope.locations[i];
          var totalFollow = 0;
          for (var j = 0; j < resp.data.locationFollow.length; j++) {
            var follow = resp.data.locationFollow[j];
            if (follow.location_id == location._id) totalFollow++;
          }
          location.totalFollow = totalFollow;
          var totalStatus = 0;
          for (var j = 0; j < resp.data.locationStatus.length; j++) {
            var status = resp.data.locationStatus[j];
            if (location._id == status.location_id) totalStatus++;
          }
          location.totalStatus = totalStatus;
          location.ratings = [];
          for (var j = 0; j < resp.data.ratings.length; j++) {
            var rate = resp.data.ratings[j];
            if (location._id == rate.location_id) {
              location.ratings.push(rate);
            }
          }
        }
      });

      $scope.getAvg = function (ratings) {
        if (ratings.length == 0) return 0;
        var total = 0;
        for (var i = 0; i < ratings.length; i++) {
          var rate = ratings[i];
          total += parseInt(rate.rate_number);
        }
        return (total/ratings.length).toFixed(1);
      }
    })




  ;

})();
