"use strict";

(function () {

  angular.module("kym.main-app.create-location", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.create-location", {
          url: "/location-create",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/create-location/create-location.html",
              controller: "create-location.ctrl"
            }
          }

        })
      ;
    }])

    .controller("create-location.ctrl", function ($scope, $ionicLoading, $cordovaGeolocation, $ionicPopup, locationApi, $locations, $state) {
      var posOptions = {timeout: 10000, enableHighAccuracy: false};

      var map;
      var infoWindow = null;
      $scope.loading = true;

      $scope.location = {};

      function initialize() {
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
        });

        $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            $scope.location.pos = pos;

            var myLatlng = new google.maps.LatLng(pos.lat,pos.lng);

            var mapOptions = {
              center: myLatlng,
              zoom: 16
            };
            var map = new google.maps.Map(document.getElementById("map"),
              mapOptions);
            infoWindow = new google.maps.InfoWindow({map: map});
            infoWindow.setPosition(pos);
            $ionicLoading.hide();
            $scope.loading = false;
            map.setCenter(pos);
          }, function(err) {
            alert(err);
          });
      }

      initialize();

      $scope.$watch("location.name", function(locationName) {
        if (infoWindow != null){
          infoWindow.setContent(locationName);
        }
      });

      $scope.createLocation = function () {
        locationApi.createLocation($scope.location).then(function (resp) {
          var alertPopup = $ionicPopup.alert({
            title: 'Gửi thành công',
            template: 'Địa điểm đã được gửi cho chúng tôi thành công. Chúng tôi sẽ xem xét và duyệt sớm nhất có thể. Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!'
          });

          alertPopup.then(function(res) {
            $state.go("main-app.setting");
          });
          //$locations.addNewLoaction(location);
        })
      }




    })




  ;

})();
