"use strict";

(function () {

  angular.module("kym.main-app.check-location", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.check-location", {
          url: "/check-location",
          views: {
            'account': {
              templateUrl: "angular/main-app/account/check-location/check-location.html",
              controller: "check-location.ctrl"
            }
          }

        })
      ;
    }])

    .controller("check-location.ctrl", function ($scope, $ionicLoading, locationApi, $user, $locationsCache, $ionicActionSheet, $ionicPopup) {
      locationApi.getCheckLocation().then(function (resp) {
        $scope.locationCheck = resp.data;
      });

      $scope.$user = $user;
      $scope.view = {};
      $scope.$locationsCache = $locationsCache;
      $scope.toggleAction = function (location) {
        if (location.info.status == 'requesting') {
          $ionicActionSheet.show({
            buttons: [
              { text: 'Duyệt <b>' + location.info.name + '</b>?' }
            ],
            destructiveText: 'Từ chối',
            buttonClicked: function(index) {
              if (index == 0) {
                locationApi.acceptLocation(location.info).then(function () {
                  _.remove($scope.locationCheck, function (l) {
                    return l.info._id == location.info._id;
                  });
                  $ionicPopup.alert({
                    title: 'Duyệt',
                    template: 'Địa điểm đã được duyệt'
                  });
                })
              }
              return true;
            },
            destructiveButtonClicked: function () {
              $ionicPopup.show({
                template: '<input type="text" ng-model="view.reason">',
                title: 'Lý do',
                subTitle: 'Tại sao bạn lại từ chối địa điểm này?',
                scope: $scope,
                buttons: [
                  { text: 'Thôi' },
                  {
                    text: '<b>Đồng ý</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                      if (!$scope.view.reason) {
                        e.preventDefault();
                        $ionicPopup.alert({
                          title: 'Cảnh báo',
                          template: 'Không được để trống lý do.'
                        });
                      } else {
                        location.info.reason = $scope.view.reason;
                        locationApi.declineLocation(location.info).then(function () {
                          location.info.status = "decline";
                        })

                      }
                    }
                  }
                ]});
              return true;
            }
          });
        } else {
          $ionicActionSheet.show({
            buttons: [
              { text: 'Duyệt lại?' }
            ],
            buttonClicked: function(index) {
              if (index == 0) {
                locationApi.acceptLocation(location.info).then(function () {
                  _.remove($scope.locationCheck, function (l) {
                    return l.info._id == location.info._id;
                  });
                  $ionicPopup.alert({
                    title: 'Duyệt',
                    template: 'Địa điểm đã được duyệt'
                  });
                })
              }
              return true;
            }
          })
        }

      }

    })




  ;

})();
