"use strict";

(function () {

  angular.module("kym.main-app", [
    "ui.router",
    "kym.main-app.newfeed",
    "kym.main-app.view-location-newfeed",
    "kym.main-app.search",
    "kym.main-app.upload-image",
    "kym.main-app.notification",
    "kym.main-app.account",
    "kym.main-app.message",
    "kym.main-app.view-image-filtered",
    "kym.main-app.upload-status",
    "kym.main-app.setting",
    "kym.main-app.view-liker",
    "kym.main-app.user-view",
    "kym.main-app.view",
    "kym.main-app.view-follow-newfeed",
    "kym.view-status-searched",
    "kym.main-app.notification-status-view",
    "kym.main-app.message-view",
    "kym.main-app.create-location",
    "kym.main-app.view-location",
    "kym.main-app.view-follow-location",
    "kym.main-app.view-follow-location-manager",
    "kym.main-app.view-ads-location",
    "kym.main-app.check-location",
    "kym.main-app.statistic-location"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app", {
          url: "/main-app",
          templateUrl: "angular/main-app/main-app.html",
          controller: "main-app.ctrl",
          abstract: true
        })
      ;
    }])

    .controller("main-app.ctrl", function ($scope, $notifications, $state, $chatSessions) {
      $scope.$notifications = $notifications;

      $scope.seenNoti = function () {
        if ($notifications.getCountNoti() > 0) {
          $notifications.seenAllNoti();
        }
        $state.go("main-app.notification");
      };
      $chatSessions.ready().then(function () {
        $scope.$chatSessions = $chatSessions;
      })
    })

  ;

})();
