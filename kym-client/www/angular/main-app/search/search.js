"use strict";

(function () {

  angular.module("kym.main-app.search", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.search", {
          url: "/search?:text",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/search/search.html",
              controller: 'search.ctrl'
            }
          }
        })
      ;
    }])

    .controller("search.ctrl", function ($scope, $ionicHistory, searchApi, $user, Api, User, $state, $stateParams, $q, $statusSearched, locationApi, $locationsCache) {
      $scope.goBack = function () {
        $ionicHistory.goBack();
      };

      $scope.Api = Api;
      $scope.goToProfile = function (userID) {
        if (User._id == userID) $state.go("main-app.account");
        else $state.go("main-app.user-view", {userID: userID});
      };

      var MapTags = function (status) {
        var listMapped = [];
        var listTags = findTagsWithKeyword($scope.keyword.toLowerCase(), status);
        listTags.sort();
        var current = null;
        var cnt = 0;
        for (var i = 0; i < listTags.length; i++) {
          if (listTags[i] != current) {
            if (cnt > 0) {
              var tag = {
                name: current,
                times: cnt
              };
              listMapped.push(tag);
            }
            current = listTags[i];
            cnt = 1;
          } else {
            cnt++;
          }
        }
        if (cnt > 0) {
          var tag = {
            name: current,
            times: cnt
          };
          listMapped.push(tag);
        }
        return listMapped;
      };

      var findTagsWithKeyword = function (keyword, status) {
        var listTags = [];
        for (var i = 0; i < status.length; i++) {
          var s = status[i];
          var tags = s.text.split(' ');
          tags = _.uniq(tags);
          for (var j = 0; j < tags.length; j++) {
            var tag = tags[j];
            if (tag[0] == '#') {
              if (Vi.removeMark(tag.toLowerCase()).indexOf('#' + keyword) > -1) {
                listTags.push(tag);
              }
            }
          }
        }
        return listTags;

      };

      var statusSearched = [];

      $scope.$watch("keyword", function (value) {
        if (!value) {
          $scope.userSearched = [];
          $scope.tagSearched = [];
          return;
        }
        $scope.loading = true;
        var searchPeople = function () {
          var defer = $q.defer();
          searchApi.searchPeople({keyword: value.toLowerCase()}).then(function (resp) {
            defer.resolve(resp.data);
          });
          return defer.promise;
        };

        var searchTag = function () {
          var defer = $q.defer();
          searchApi.searchTag({keyword: value.toLowerCase()}).then(function (resp) {
            angular.copy(resp.data, statusSearched);
            defer.resolve(MapTags(resp.data));
          });
          return defer.promise;
        };

        var searchLocation = function () {
          var defer = $q.defer();
          locationApi.searchLocation(value.toLowerCase()).then(function (resp) {
            defer.resolve(resp.data);
          });
          return defer.promise;
        };

        $q.all([
          searchPeople(),
          searchTag(),
          searchLocation()
        ]).then(function (resp) {
          $scope.userSearched = resp[0];
          $scope.tagSearched = resp[1];
          $scope.locationSearched = resp[2];
          $scope.loading = false;
        })


      });


      $scope.FilterSearch = [
        {name: 'Tên người dùng'},
        {name: 'HashTag'},
        {name: 'Địa danh'}
      ];

      $scope.$locationsCache = $locationsCache;

      if ($stateParams.text) {
        $scope.keyword = $stateParams.text;
        $scope.selected = $scope.FilterSearch[1];
      } else {
        $scope.selected = $scope.FilterSearch[0];
      }


      $scope.goStatusSearched = function (tag) {
        var statusFiltered = _.filter(statusSearched, function (s) {
          var text = s.text.split(' ');
          for (var i = 0; i < text.length; i++) {
            var t = text[i];
            if (t[0] == '#') {
              if (t == tag) {
                return true;
              }
            }
          }
          return false;
        });
        $statusSearched.addStatusSearched(statusFiltered).then(function () {
          $state.go("main-app.view-status-searched", {keyword: tag});
        });
      }


    })

  ;

})();
