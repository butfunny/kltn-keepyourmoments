"use strict";

(function () {

    angular.module('kym.view-status-searched', [
        'ui.router'
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state('main-app.view-status-searched', {
                    url: '/view-status-searched?:keyword',
                    views: {
                      'newfeed': {
                        templateUrl: "angular/main-app/search/view-status-searched/view-status-searched.html",
                        controller: "view-status-searched.ctrl"
                      }
                    }
                })
            ;
        }])

        .controller("view-status-searched.ctrl", function($scope, $stateParams, $statusSearched, $user, Api) {
          $scope.tagName = $stateParams.keyword;
          $scope.status = $statusSearched.getStatusSearched();
          $scope.$user = $user;
          $scope.Api = Api;


      })

    ;

})();
