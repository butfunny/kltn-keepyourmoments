"use strict";

(function () {

  angular.module("kym.main-app.user-view", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.user-view", {
          url: "/user-view?:userID",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/user-view/user-view.html",
              controller: "user-view.ctrl"
            }
          }
        })
      ;
    }])

    .controller("user-view.ctrl", function ($scope, $stateParams, userApi, $q, statusApi, $ionicLoading, Api, $follow, followApi, $user, $state, User, commentApi, $ionicModal, $ionicActionSheet, $locationsCache) {
      var initUser = function (userID) {
        var defer = $q.defer();
        userApi.getUserById(userID).then(function (resp) {
          $scope.userView = resp.data;
          defer.resolve();
        });
        return defer.promise;
      };

      var initStatus = function (userID) {
        var defer = $q.defer();
        statusApi.getStatusByUserId(userID).then(function (resp) {
          var status = resp.data;
          var statusIds = _.map(status, '_id');
          Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
            for (var i = 0; i < status.length; i++) {
              var s = status[i];
              s.likes = [];
              for (var j = 0; j < respLike.data.length; j++) {
                var like = respLike.data[j];
                if (like.status_id == s._id) {
                  s.likes.push(like);
                }
              }
            }
            Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
              for (var i = 0; i < status.length; i++) {
                var s = status[i];
                s.comments = [];
                for (var j = 0; j < respComments.data.length; j++) {
                  var comment = respComments.data[j];
                  if (comment.status_id == s._id) {
                    s.comments.unshift(comment);
                  }
                }
              }
              $scope.status = angular.copy(status);
              defer.resolve();
            })
          });
        });
        return defer.promise;
      };

      var initFollow = function (userID) {
        var defer = $q.defer();
        followApi.getFollowByUserID(userID).then(function (resp) {
          $scope.following = resp.data.length;
          defer.resolve();
        });
        return defer.promise;
      };

      var initFollower = function (userID) {
        var defer = $q.defer();
        followApi.getFollowerByUserID(userID).then(function (resp) {
          $scope.follower = resp.data.length;
          defer.resolve();
        });
        return defer.promise;
      };

      $scope.refresh = function () {
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
        });
        $q.all([
          initUser($stateParams.userID),
          initStatus($stateParams.userID),
          initFollow($stateParams.userID),
          initFollower($stateParams.userID)
        ]).then(function () {
          $ionicLoading.hide();
        });
      };

      $scope.refresh();


      $scope.Api = Api;
      $scope.$follow = $follow;
      $scope.$user = $user;

      $scope.follow = function (user_id) {
        followApi.follow(user_id);
        $scope.follower++;
      };

      $scope.unFollow = function (user_id) {
        $ionicActionSheet.show({
          destructiveText: 'Bỏ theo dõi',
          cancelText: 'Hủy',
          destructiveButtonClicked: function () {
            followApi.unFollow($follow.getFollow(user_id)._id);
            $scope.follower--;
            return true;
          }
        });
      };


    })

  ;

})();
