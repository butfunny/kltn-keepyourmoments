"use strict";

(function () {

  angular.module("kym.main-app.view-ads-location", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view-ads-location", {
          url: "/view-ads-location/:status_id",
          views: {
            'newfeed': {
              templateUrl: "/angular/main-app/newfeed/view-ads-location/view-ads-location.html",
              controller: "view-ads-location.ctrl"
            }
          }
        })
      ;
    }])

    .controller("view-ads-location.ctrl", function ($scope, locationApi, $stateParams, Api, $listUsersView, $state) {
      $scope.ready = false;
      locationApi.getStatusLocation($stateParams.status_id).then(function (resp) {
        $scope.s = resp.data;
        Api.post("/api/location/like/prepare", {ids: $stateParams.status_id}).then(function (respLike) {
          $scope.s.likes = respLike.data;
          Api.post("/api/location/comment/prepare", {ids: $stateParams.status_id}).then(function (respComments) {
            $scope.s.comments = respComments.data;
            $scope.ready = true;
          })
        })
      });

      $scope.viewLiker = function (status) {
        $listUsersView.setList(status.likes);
        $state.go("main-app.view-follow-location", {type: 'like'});
      };


    })

  ;

})();
