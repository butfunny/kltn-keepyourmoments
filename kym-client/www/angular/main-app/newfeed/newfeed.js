"use strict";

(function () {

  angular.module("kym.main-app.newfeed", [
    "ui.router",
    "ionicLazyLoad"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.newfeed", {
          url: "/newfeed",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/newfeed/newfeed.html",
              controller: "newfeed.ctrl"
            }
          }

        })
      ;
    }])

    .controller("newfeed.ctrl", function ($scope, $user, $status, $follow, followApi, Api, User, SocketIO, $q, $listUsersView, $state, $cordovaGeolocation, statusApi, $locationsCache) {

      $scope.$locationsCache = $locationsCache;

      $status.ready().then(function () {
        $scope.status = $status.getAllStatus();

        $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            statusApi.getAds(pos).then(function (resp) {
              var statusAds = resp.data;
              for (var i = 0; i < statusAds.length; i++) {
                var s = statusAds[i];
                s.ads = true;
                var randomIndex = Math.floor(Math.random() * $scope.status.length) + 1;
                $status.insertStatus(s, randomIndex);
              }
            }, function (err) {
              console.log(err);
            })
          });
      });

      var posOptions = {timeout: 10000, enableHighAccuracy: false};

      SocketIO.onAlways("need-reload-image", function () {
        $scope.accountView.reloadImage();
      });

      $scope.doRefresh = function () {
        $status.reload().then(function () {
          $scope.$broadcast('scroll.refreshComplete');
        });
      };

      $scope.Api = Api;
      $scope.User = User;

      $follow.ready().then(function () {
        if ($follow.getMyFollower().length == 0) {
          followApi.getMySuggestFollow().then(function (resp) {
            $scope.suggestUsersFollow = resp.data;
          })
        }
      });

      $scope.followUser = function (user) {
        followApi.follow(user._id);
        _.remove($scope.suggestUsersFollow, function (s) {
          return s._id == user._id
        });
      };

      $scope.viewLiker = function (status) {
        $listUsersView.setList(status.likes);
        $state.go("main-app.view-follow-location", {type: 'like'});
      };



    })


  ;

})();
