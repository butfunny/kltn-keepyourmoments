"use strict";

(function () {

  angular.module("kym.main-app.view-follow-location", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view-follow-location", {
          url: "/view-follow-location/:type",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/newfeed/view-location/view-like-and-follow-location/view-like-and-follow-location.html",
              controller: "view-follow-newfeed-location.ctrl"
            }
          }
        })
      ;
    }])

    .controller("view-follow-newfeed-location.ctrl", function($scope, $stateParams, $status, Api, $user, User, $follow, followApi, $ionicActionSheet, $state, $listUsersView, $locationsCache) {
      if ($stateParams.type == 'like') {
        $scope.title = "Người thích";
      }

      if ($stateParams.type == 'follow') {
        $scope.title = "Người theo dõi";
      }

      $scope.$listUsersView = $listUsersView;
      $scope.$locationsCache = $locationsCache;

      $scope.Api = Api;
      $scope.$user = $user;
      $scope.User = User;
      $scope.$follow = $follow;

      $scope.follow = function (user_id) {
        followApi.follow(user_id);
      };

      $scope.unFollow = function (user_id) {
        $ionicActionSheet.show({
          destructiveText: 'Bỏ theo dõi',
          cancelText: 'Hủy',
          destructiveButtonClicked: function() {
            followApi.unFollow($follow.getFollow(user_id)._id);
            return true;
          }
        });
      };

      $scope.goToProfile = function (userID) {
        if (User._id == userID) $state.go("main-app.account");
        else $state.go("main-app.user-view", {userID: userID});
      };


      $scope.getAvatarUrl = function (user) {
        var user_id = user.user_id;
        if ($stateParams.type == 'follow'){
          user_id = user.user_follow;
        }
        return $user.getUser(user_id).avatar ?
          (Api.getServerUrl() + '/uploads/avatar/' + $user.getUser(user_id).avatar_url) :
        'https://graph.facebook.com/ ' + $user.getUser(user_id).facebook_id + '/picture?type=large';
      };

      $scope.getAvatarLocation = function (user) {
        var user_id = user.user_id;
        if ($stateParams.type == 'follow'){
          user_id = user.user_follow;
        }
        var location = $locationsCache.getLocation(user_id);
        return location.avatar ? Api.getServerUrl() + '/uploads/avatar/' + location.avatar : '/img/location.png';
      };


      $scope.getUserID = function (user) {
        if ($stateParams.type == 'follow'){
          return user.user_follow;
        }
        return user.user_id
      }

    })

  ;

})();
