"use strict";

(function () {

  angular.module("kym.main-app.view-location-newfeed", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view-location-newfeed", {
          url: "/view-location-newfeed/:locationId",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/newfeed/view-location/view-location.html",
              controller: "viewLocation.ctrl"
            }
          }

        })
      ;
    }])


    .factory("$listUsersView", function () {
      var listUsersView = [];
      return {
        setList: function (users) {
          angular.copy(users, listUsersView);
        },
        getList: function () {
          return listUsersView;
        }
      };
    })

    .controller("viewLocation.ctrl", function ($scope, locationApi, $q, $stateParams, $ionicLoading, Api, $locationsCache, User, $ionicPopup, $ionicActionSheet, $listUsersView, $state, $status) {

      $scope.location = angular.copy($locationsCache.getLocation($stateParams.locationId));
      $scope.Api = Api;

      var lat = parseFloat($scope.location.pos.lat);
      var lng = parseFloat($scope.location.pos.lng);

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: {
          lat: lat,
          lng: lng
        }
      });

      var marker = new google.maps.Marker({
        position: {
          lat: lat,
          lng: lng
        },
        map: map,
        title: $scope.location.name
      });


      $scope.isOpenRateDetail = false;
      $scope.openRateDetail = function () {
        $scope.isOpenRateDetail = !$scope.isOpenRateDetail;
      };

      $scope.isFollow = function () {
        if ($scope.follower) {
          for (var i = 0; i < $scope.follower.length; i++) {
            var person = $scope.follower[i];
            if (person.user_follow == User._id) {
              return true;
            }
          }
        }
        return false;
      };

      $scope.toggleFollow = function () {
        if ($scope.isFollow()) {
          $ionicActionSheet.show({
            destructiveText: 'Bỏ theo dõi',
            cancelText: 'Hủy',
            destructiveButtonClicked: function () {
              locationApi.unFollowLocation($scope.location._id).then(function () {
                _.remove($scope.follower, function (f) {
                  return f.user_follow == User._id;
                });
              });
              $status.reload();
              return true;

            }
          })
        } else {
          locationApi.followLocation($scope.location._id).then(function (resp) {
            $scope.follower.push(resp.data);
            $status.reload();
          })
        }
      };

      $scope.location.canRate = true;

      var getRatingInfo = function () {
        var defer = $q.defer();
        locationApi.getRating($scope.location._id).then(function (resp) {
          $scope.location.rating = resp.data;
          defer.resolve();
        });
        return defer.promise;
      };

      var getFollowInfo = function () {
        var defer = $q.defer();
        locationApi.getFollow($scope.location._id).then(function (resp) {
          $scope.follower = resp.data;
          defer.resolve();
        });
        return defer.promise;
      };


      var prepareLocationPost = function (locationPosts) {
        var defer = $q.defer();
        $scope.locationPosts = locationPosts;
        var statusLocationIds = _.map($scope.locationPosts, "_id");
        Api.post("/api/location/like/prepare", {ids: statusLocationIds}).then(function (respLike) {
          for (var i = 0; i < $scope.locationPosts.length; i++) {
            var s = $scope.locationPosts[i];
            s.likes = [];
            for (var j = 0; j < respLike.data.length; j++) {
              var like = respLike.data[j];
              if (like.status_location_id == s._id) {
                s.likes.push(like);
              }
            }
          }
          Api.post("/api/location/comment/prepare", {ids: statusLocationIds}).then(function (respComments) {
            for (var i = 0; i < $scope.locationPosts.length; i++) {
              var s = $scope.locationPosts[i];
              s.comments = [];
              for (var j = 0; j < respComments.data.length; j++) {
                var comment = respComments.data[j];
                if (comment.status_location_id == s._id) {
                  s.comments.unshift(comment);
                }
              }
            }
            defer.resolve();
          })
        });

        return defer.promise;
      };

      var prepareUserPost = function (userPosts) {
        var defer = $q.defer();
        $scope.userPosts = userPosts;
        var statusIds = _.map($scope.userPosts, '_id');
        Api.post("/api/status/like/prepare", {status_ids: statusIds}).then(function (respLike) {
          for (var i = 0; i < $scope.userPosts.length; i++) {
            var s = $scope.userPosts[i];
            s.likes = [];
            for (var j = 0; j < respLike.data.length; j++) {
              var like = respLike.data[j];
              if (like.status_id == s._id) {
                s.likes.push(like);
              }
            }
          }
          Api.post("/api/comment/prepare", {status_ids: statusIds}).then(function (respComments) {
            for (var i = 0; i < $scope.userPosts.length; i++) {
              var s = $scope.userPosts[i];
              s.comments = [];
              for (var j = 0; j < respComments.data.length; j++) {
                var comment = respComments.data[j];
                if (comment.status_id == s._id) {
                  s.comments.unshift(comment);
                }
              }
            }
            defer.resolve();
          })
        });
        return defer.promise;

      };


      var getPosts = function () {
        var defer = $q.defer();
        locationApi.getPosts($scope.location._id).then(function (resp) {
          $q.all([
            prepareLocationPost(resp.data.location),
            prepareUserPost(resp.data.users)
          ]).then(function () {
            defer.resolve();
          })

        });
        return defer.promise;
      };


      $scope.getAvgRating = function () {
        if (!$scope.location.rating) return 0;
        var total = 0;

        for (var i = 0; i < $scope.location.rating.length; i++) {
          var rate = $scope.location.rating[i];
          total += parseInt(rate.rate_number);
          if (rate.user_id == User._id) {
            $scope.location.canRate = false;
          }
        }

        if ($scope.location.rating.length == 0) {
          return 0;
        } else {
          return (total / $scope.location.rating.length).toFixed(1);
        }
      };

      $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
      });
      $q.all([
        getRatingInfo(),
        getFollowInfo(),
        getPosts()
      ]).then(function () {
        $ionicLoading.hide();
      });

      $scope.view = {};

      $scope.rateLocation = function () {
        var rateInfo = {
          rate_number: $scope.view.myRating,
          location_id: $scope.location._id
        };
        locationApi.rateLocation(rateInfo).then(function (resp) {
          var alertPopup = $ionicPopup.alert({
            title: 'Cảm ơn!',
            template: 'Bạn đã đánh giá ' + $scope.view.myRating + ' sao cho <b>' + $scope.location.name + '</b>'
          });

          alertPopup.then(function (res) {
            $scope.location.canRate = false;
            $scope.location.rating.push(resp.data);
          });
        })
      };

      $scope.options = ["Bài đăng của địa điểm", "Bài đăng của người dùng"];
      $scope.selected = $scope.options[0];

      $scope.viewFollower = function () {
        $listUsersView.setList($scope.follower);
        $state.go("main-app.view-follow-location", {type: 'follow'});
      };

      $scope.viewLiker = function (status) {
        $listUsersView.setList(status.likes);
        $state.go("main-app.view-follow-location", {type: 'like'});
      };

      $scope.optionsSort = ["Thời gian", "Lượng thích"];
      $scope.view.selectedSort = $scope.optionsSort[0];

      $scope.$watch("view.selectedSort", function(selectedSort) {
          if ($scope.userPosts) {
            if (selectedSort == "Thời gian") {
              $scope.userPosts.sort(function (a, b) {
                return a.created > b.created ? 1 : -1;
              });
            } else {
              $scope.userPosts.sort(function (a, b) {
                return b.likes.length - a.likes.length;
              });
            }
          }
      });



    })


  ;

})();
