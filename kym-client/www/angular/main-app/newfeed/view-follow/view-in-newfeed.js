"use strict";

(function () {

  angular.module("kym.main-app.view-follow-newfeed", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.view-follow-newfeed", {
          url: "/view-newfeed/:type/:view_id",
          views: {
            'newfeed': {
              templateUrl: "angular/main-app/newfeed/view-follow/view-in-newfeed.html",
              controller: "view-follow-newfeed.ctrl"
            }
          }
        })
      ;
    }])

    .controller("view-follow-newfeed.ctrl", function($scope, $stateParams, $status, Api, $user, User, $follow, followApi, $ionicActionSheet, $state, $follower, statusApi) {
      if ($stateParams.type == 'like') {
        statusApi.getLikeUsersStatusID($stateParams.view_id).then(function (resp) {
           $scope.listUserView = resp.data;
        });
      }

      if ($stateParams.type == 'follow') {
         followApi.getFollowByUserID($stateParams.view_id).then(function (resp) {
           $scope.listUserView = resp.data;
        });
      }

      if ($stateParams.type == 'follower') {
        followApi.getFollowerByUserID($stateParams.view_id).then(function (resp) {
          $scope.listUserView = resp.data;
        });

      }

      $scope.Api = Api;
      $scope.$user = $user;
      $scope.User = User;
      $scope.$follow = $follow;

      $scope.follow = function (user_id) {
        followApi.follow(user_id);
      };

      $scope.unFollow = function (user_id) {
        $ionicActionSheet.show({
          destructiveText: 'Bỏ theo dõi',
          cancelText: 'Hủy',
          destructiveButtonClicked: function() {
            followApi.unFollow($follow.getFollow(user_id)._id);
            return true;
          }
        });
      };

      $scope.goToProfile = function (userID) {
        if (User._id == userID) $state.go("main-app.account");
        else $state.go("main-app.user-view", {userID: userID});
      };

      $scope.getAvatarUrl = function (user) {
        var user_id = user.user_id;
        if ($stateParams.type == 'follow'){
           user_id = user.user_to_follow;
        }
        return $user.getUser(user_id).avatar ?
          (Api.getServerUrl() + '/uploads/avatar/' + $user.getUser(user_id).avatar_url) :
          'https://graph.facebook.com/ ' + $user.getUser(user_id).facebook_id + '/picture?type=large';
      };

      $scope.getUserID = function (user) {
        if ($stateParams.type == 'follow'){
          return user.user_to_follow;
        }
        return user.user_id
      }


    })

  ;

})();
