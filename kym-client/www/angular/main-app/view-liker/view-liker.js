"use strict";

(function () {

    angular.module("kym.main-app.view-liker", [
        "ui.router"
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state("main-app.view-liker", {
                    url: "/view-liker?:status_id",
                    views: {
                      'newfeed': {
                        templateUrl: "angular/main-app/view-liker/view-liker.html",
                        controller: "view-liker.ctrl"
                      }
                    }
                })
            ;
        }])

        .controller("view-liker.ctrl", function($scope, $stateParams, $status, $user, Api, User, $follow, followApi, $ionicActionSheet, $state) {
            $scope.listLiker = $status.getLike($stateParams.status_id);
            $scope.Api = Api;
            $scope.$user = $user;
            $scope.User = User;
            $scope.$follow = $follow;

            $scope.follow = function (user_id) {
              followApi.follow(user_id);
            };

            $scope.unFollow = function (user_id) {
              $ionicActionSheet.show({
                destructiveText: 'Bỏ theo dõi',
                cancelText: 'Hủy',
                destructiveButtonClicked: function() {
                  followApi.unFollow($follow.getFollow(user_id)._id);
                  return true;
                }
              });
            };

            $scope.goToProfile = function (userID) {
              if (User._id == userID) $state.go("main-app.account");
              else $state.go("main-app.user-view", {userID: userID});
            };
        })

    ;

})();
