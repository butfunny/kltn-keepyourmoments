"use strict";

(function () {

  angular.module("kym.main-app.upload-status", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.upload-status", {
          url: "/upload-status?:type_image",
          views: {
            'upload-image': {
              templateUrl: "angular/main-app/upload-image/upload-status/upload-status.html",
              controller: "upload-status.ctrl"
            }
          }
        })
      ;
    }])

    .controller("upload-status.ctrl", function ($scope, $stateParams, Api, User, statusApi, $ionicLoading, $state, $ionicHistory, $ionicModal, $faceDetect, $cordovaGeolocation, locationApi, $locations) {

      if (!$stateParams.type_image) {
        $state.go("main-app.upload-image");
      }


      $scope.status = {
        image: $stateParams.type_image,
        text: ''
      };

      $scope.postStatus = function () {
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
        });
        $scope.status.location_id = $scope.view.locationSuggested.info._id;
        var selectedIds = [];
        for (var key in $scope.selection.ids) {
          if ($scope.selection.ids[key]) {
            selectedIds.push(key);
          }
        }
        $scope.status.selection = selectedIds;
        statusApi.postStatus($scope.status).then(function () {
          $ionicLoading.hide();
          $ionicHistory.clearHistory();
          $state.go("main-app.newfeed");
        })
      };

      $scope.randomNumber = new Date().getTime();


      $scope.closeModal = function () {
        $scope.modalImageView.hide();
      };

      $scope.viewTagsImage = function () {
        $ionicModal.fromTemplateUrl('angular/main-app/upload-image/upload-status/add-tag-image.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          $scope.modalImageView = modal;
          $scope.modalImageView.show();
        });
      };

      var posOptions = {timeout: 10000, enableHighAccuracy: false};

      $scope.view = {};

      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          locationApi.suggestNearlyLocation(pos).then(function (resp) {
            $scope.locations = resp.data;
            $scope.locations.sort(function (a, b) {
              if (a.distance > b.distance) {
                return 1;
              }
              if (a.distance < b.distance) {
                return -1;
              }
              return 0;
            });

            $scope.view.locationSuggested = $scope.locations[0];
          }, function (err) {
            alert("Không thể lấy được toạ độ");
          })
        });

      $scope.$locations = $locations;


      $scope.isSelected = function () {
        for (var i = 0; i < _.keys($scope.selection.ids).length; i++) {
          var obj = _.keys($scope.selection.ids)[i];
          if ($scope.selection.ids[obj]) {
            return true;
          }
        }
        return false;
      };

      $scope.selection = {
        ids: {myProfile: true}
      };

      $scope.changeLocation = function () {
        $ionicModal.fromTemplateUrl("angular/main-app/upload-image/upload-status/add-more-location.html", {
          scope: $scope,
          animation: "slide-in-up"
        }).then(function(modal) {
          $scope.modalLocation = modal;
          $scope.modalLocation.show();
        });
      };

      $scope.$watch("view.keyword", function(value) {
        if (!value){
          $scope.locationSearched = [];
          return;
        }
        $scope.loading = true;
        locationApi.searchLocation(value.toLowerCase()).then(function (resp) {
          $scope.locationSearched = resp.data;
          $scope.loading = false;
        });
      });

      $scope.addLocation = function (location) {
        var l = _.find($scope.locations , function (l) {
          return l.info._id == location._id
        });
        if (l) {
          $scope.view.locationSuggested = l;
        } else {
          var location2 = {
            info: location
          };
          $scope.locations.push(location2);
          $scope.view.locationSuggested = location2;
        }
        $scope.modalLocation.hide();
      };




      $scope.Api = Api;
      $scope.User = User;
    })

  ;

})();
