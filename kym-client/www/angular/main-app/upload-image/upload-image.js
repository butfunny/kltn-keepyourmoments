"use strict";

(function () {

  angular.module("kym.main-app.upload-image", [
    "ui.router"
  ])

    .config(["$stateProvider", function ($stateProvider) {

      $stateProvider
        .state("main-app.upload-image", {
          url: "/upload-image",
          views: {
            'upload-image': {
              templateUrl: "angular/main-app/upload-image/upload-image.html",
              controller: "upload-image.ctrl"
            }
          }
        })
      ;
    }])

    .controller("upload-image.ctrl", function ($scope, Api, $state, $ionicLoading, $ionicPopup, $cordovaGeolocation, locationApi) {

      $scope.view = {};
      $scope.uploadImage = function () {
        if ($scope.view.imageUpload) {
          $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
          });
          Api.upload("/api/upload/upload-new-image", $scope.view.imageUpload, {filter: true}).then(function (resp) {
            $ionicLoading.hide();
            if (resp.data.error) {
              $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Có vẻ có chút vấn đề khi upload. Vui lòng thử lại,'
              });
            } else {
              $state.go("main-app.view-image-filtered");
            }
          })
        }
      };

      $scope.useOriginImage = function () {
        if ($scope.view.imageUpload) {
          $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="spinner-calm"></ion-spinner>'
          });
          Api.upload("/api/upload/upload-new-image", $scope.view.imageUpload, {filter: false}).then(function (resp) {
            $ionicLoading.hide();
            if (resp.data.error) {
              $ionicPopup.alert({
                title: 'Lỗi',
                template: 'Có vẻ có chút vấn đề khi upload. Vui lòng thử lại,'
              });
            } else {
              delete $scope.view.imageUpload;
              $state.go("main-app.upload-status", {type_image: "origin"});
            }
          })
        }
      }
    })

  ;

})();
