"use strict";

(function () {

    angular.module("kym.main-app.view-image-filtered", [
        "ui.router"
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state("main-app.view-image-filtered", {
                    url: "/view-image-filtered",
                    views: {
                      'upload-image': {
                        templateUrl: "angular/main-app/upload-image/view-image-filtered/view-image-filtered.html",
                        controller: "view-image-filtered.ctrl"
                      }
                    }

                })
            ;
        }])

        .controller("view-image-filtered.ctrl", function($scope, $ionicSlideBoxDelegate, User, ApiEndPoint, $state) {
          $scope.images = [
            {
              filter: "pleasant",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_pleasant.png?" + new Date().getTime()
            },
            {
              filter: "vintage",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_vintage.png?" + new Date().getTime()
            },
            {
              filter: "lomo",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_lomo.png?" + new Date().getTime()
            },
            {
              filter: "clarity",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_clarity.png?" + new Date().getTime()
            },
            {
              filter: "orangepeel",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_orangepeel.png?" + new Date().getTime()
            },
            {
              filter: "sunrise",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_sunrise.png?" + new Date().getTime()
            },{
              filter: "crossprocess",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_crossprocess.png?" + new Date().getTime()
            },
            {
              filter: "hazyDays",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_hazyDays.png?" + new Date().getTime()
            },
            {
              filter: "jarques",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_jarques.png?" + new Date().getTime()
            },{
              filter: "pinhole",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_pinhole.png?" + new Date().getTime()
            },
            {
              filter: "oldboot",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_oldboot.png?" + new Date().getTime()
            },
            {
              filter: "glowingSun",
              src: ApiEndPoint.url + "/uploads/image-draft/" + User._id + "_glowingSun.png?" + new Date().getTime()
            }
          ];

          $scope.next = function () {
            $state.go("main-app.upload-status", {type_image: $scope.images[$ionicSlideBoxDelegate.currentIndex()].filter})
          }
        })



    ;

})();
