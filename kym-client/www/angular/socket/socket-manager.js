"use strict";

(function () {

    angular.module("kym.socket-manager", [
    ])
        .factory("SocketIO", function(Api, $window, $rootScope, Security) {
          var socket = io(Api.getServerUrl());
          function registerListener(h) {
            socket.on(h.eventName, function () {
              var args = arguments;
              $rootScope.$apply(function () {
                h.callback.apply(null, args);
              });
            });
          }

          Security.onLogin(
            function() {
              socket.emit("loggedIn", {token: $window.localStorage.token});
            },
            function(oldToken) {
              socket.emit("loggedOut", {token: oldToken});
            }
          );
            return {
              onAlways: function (eventName, callback) {
                var h = {
                  eventName: eventName,
                  callback: callback
                };
                registerListener(h);
              }
            };
          })

      .run(function (SocketIO, $window, Api, Security) {
        SocketIO.onAlways('reconnect', function () {
          var socket = io(Api.getServerUrl());
          socket.emit("loggedIn", {token: $window.localStorage.token});
          Security.resetDataApp();
        })
      })
    ;

})();
