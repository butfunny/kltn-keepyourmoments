"use strict";

(function () {

    angular.module("kym.facebook-api", [
    ])
        .factory("FacebookApi", function($q, Security) {

            var getFacebookProfileInfo = function (authResponse) {
              var info = $q.defer();
              facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null ,
                function (response) {
                    info.resolve(response);
                },
                function (response) {
                  info.reject(response);
                }
              );
              return info.promise;
            };

            var fbLoginSuccess = function(response) {
              if (!response.authResponse){
                alert("Lỗi kết nối với Facebook");
                return;
              }

              var authResponse = response.authResponse;

              getFacebookProfileInfo(authResponse)
                .then(function(profileInfo) {
                  Security.loginFacebook(profileInfo);
                }, function(fail){

                });
            };

            return {
                login: function () {
                  facebookConnectPlugin.getLoginStatus(function(success){
                    if(success.status === 'connected'){
                      getFacebookProfileInfo(success.authResponse)
                        .then(function(profileInfo) {
                          Security.loginFacebook(profileInfo);
                        }, function(){
                          alert("Lỗi kết nối với Facebook");
                        });

                    } else {
                      facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, function () {
                        alert("Lỗi kết nối với Facebook");
                      });
                    }
                  });
                }
            };
        })
    ;

})();
