// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js




angular.module('kym-app',
  [
    'ionic',
    'ngFileUpload',
    'ngCordova',

    'kym.main-app',
    'kym.login-facebook',
    'kym.update-information',

    'kym.api',
    'kym.security',
    'kym.facebook-api',
    'kym.api.status-api',
    'kym.follow-api',
    'kym.comment-api',
    'kym.api.user-api',
    'kym.search-api',
    'kym.location-api',

    'kym.layout',

    'kym.cache-support',
    'kym.status-support',
    'kym.follow-support',
    'kym.user-cache',
    'kym.theme',
    'kym.status-nav',
    'kym.follower-support',
    'kym.status-searched-support',
    'kym.notification-support',
    'kym.message-support',
    'kym.location-support',
    'kym.message-api',

    'kym.socket-manager'
  ])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
  .constant("ApiEndPoint", {
    url : 'http://192.168.5.222:3000'
  })

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    //$urlRouterProvider.otherwise('/login-facebook');
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.views.swipeBackEnabled(false);

  });
