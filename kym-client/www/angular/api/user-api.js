"use strict";

(function () {

    angular.module("kym.api.user-api", [
    ])
        .factory("userApi", function(Api) {
            return {
                getUserById: function (id) {
                  return Api.get("/api/user/getById/" + id);
                }
            };
        })
    ;

})();
