"use strict";

(function () {

    angular.module("kym.follow-api", [
    ])
        .factory("followApi", function(Api) {
            return {
                follow: function (user_id) {
                  return Api.post("/api/follow", {user_id: user_id});
                },
                unFollow: function (followId) {
                  return Api.delete("/api/unfollow/" + followId);
                },
                getMySuggestFollow : function () {
                  return Api.get("/api/follow/suggest");
                },
                getFollowByUserID: function (userID) {
                  return Api.get("/api/follow/byUserID/" + userID);
                },
                getFollowerByUserID: function (userID) {
                  return Api.get("/api/follower/byUserID/" + userID);
                }
            };
        })
    ;

})();
