"use strict";

(function () {

    angular.module("kym.comment-api", [
    ])
        .factory("commentApi", function(Api) {
            return {
                comment: function (comment) {
                  return Api.post("/api/comment", comment);
                },
                remove: function (id) {
                  return Api.delete("/api/comment/" + id);
                }
            };
        })
    ;

})();
