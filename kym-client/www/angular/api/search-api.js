"use strict";

(function () {

    angular.module("kym.search-api", [
    ])
        .factory("searchApi", function(Api) {
            return {
                searchPeople: function (keyword) {
                  return Api.post("/api/search/people", keyword);
                },
                searchTag: function (keyword) {
                  return Api.post("/api/search/tag", keyword);
                }
            };
        })
    ;

})();
