"use strict";

(function () {

  angular.module("kym.location-api", [
  ])
    .factory("locationApi", function(Api) {
      return {
        createLocation : function (location) {
          return Api.post("/api/location", location);
        },
        getLocation: function (id) {
          return Api.get("/api/location/" + id);
        },
        getRating: function (id) {
          return Api.get("/api/location/rating/" + id);
        },
        getFollow: function (id) {
          return Api.get("/api/location/follow/" + id);
        },
        followLocation : function (id) {
          return Api.post("/api/location/follow/" + id );
        },
        unFollowLocation : function (id) {
          return Api.delete("/api/location/un-follow/" + id );
        },
        getPosts: function (id) {
          return Api.get("/api/location/posts/" + id);
        },
        rateLocation: function (rateInfo) {
          return Api.post("/api/location/rating", rateInfo);
        },
        suggestNearlyLocation: function (pos) {
          return Api.post("/api/location/suggest", pos);
        },
        likeStatus: function (id) {
          return Api.post("/api/location/like-status/" + id);
        },
        likeStatusWithManager: function (id, location_id) {
          return Api.post("/api/location/like-status-manager/" + id, {location_id: location_id});
        },
        unlikeStatus: function (id) {
          return Api.delete("/api/location/unlike-status/" + id);
        },
        unlikeStatusWithManager: function (id, location_id) {
          return Api.delete("/api/location/unlike-status-manager/" + id, {location_id: location_id});
        },
        commentStatus: function (comment) {
          return Api.post("/api/location/comment-status", comment);
        },
        commentStatusWithManager: function (comment) {
          return Api.post("/api/location/comment-status-manager", comment);
        },
        deleteStatus: function (sid) {
          return Api.delete("/api/location/status/" + sid);
        },
        adsStatus: function (info) {
          return Api.post("/api/location/ads" , info);
        },
        getStatusLocation: function (status_id) {
          return Api.get("/api/location/status-only/" + status_id);
        },
        getCheckLocation: function () {
          return Api.get("/api/location/checking");
        },
        acceptLocation: function (location) {
          return Api.post("/api/location/accept-location", location);
        },
        declineLocation: function (location) {
          return Api.put("/api/location/decline-location", location);
        },
        searchLocation: function (keyword) {
          return Api.get("/api/location/search/" + keyword);
        },
        getStatisticLocation: function () {
          return Api.get("/api/location/statistic");
        }
      };
    })
  ;

})();
