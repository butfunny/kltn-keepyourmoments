"use strict";

(function () {

  angular.module("kym.message-api", [
  ])
    .factory("messageApi", function(Api) {
      return {
        sendMessage: function (message) {
          return Api.post("/api/message/send", message);
        },
        seenSession: function (session_id) {
          return Api.put("/api/message/seen/" + session_id);
        }
      };
    })
  ;

})();
