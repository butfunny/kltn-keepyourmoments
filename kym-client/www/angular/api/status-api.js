"use strict";

(function () {

    angular.module("kym.api.status-api", [
    ])
        .factory("statusApi", function(Api) {
            return {
                postStatus: function (status) {
                  return Api.post("/api/new-status", status);
                },
                getStatusMe: function () {
                  return Api.get("/api/status/me");
                },
                likeStatus: function (status_id) {
                  return Api.post("/api/status/like", {status_id: status_id});
                },
                unlikeStatus: function (status_id) {
                  return Api.post("/api/status/unlike", {status_id: status_id});
                },
                deleteStatus: function (status_id) {
                  return Api.delete("/api/status/delete/" + status_id);
                },
                updateStatus: function (status) {
                  return Api.put("/api/status/update", status);
                },
                getStatusByUserId: function (uid) {
                  return Api.get("/api/status/byUserID/" + uid);
                },
                getLikeUsersStatusID: function (sid) {
                  return Api.get("/api/status/like/" + sid);
                },
                getAds: function (info) {
                  return Api.post("/api/status/ads", info);
                }
            };
        })
    ;

})();
