"use strict";

(function () {

    angular.module("kym.login-facebook", [
        "ui.router"
    ])

        .config(["$stateProvider", function ($stateProvider) {

            $stateProvider
                .state("login-facebook", {
                    url: "/login-facebook",
                    templateUrl: "angular/login-facebook/login-facebook.html",
                    controller: "login-facebook.ctrl"
                })
            ;
        }])

        .controller("login-facebook.ctrl", function($scope, FacebookApi, Security, $state, Api, $ionicPopup) {
            $scope.user = {
              //user_name: "butfunny",
              //password: "123123"
            };
            $scope.loginNormal = function () {
              Security.loginPassword($scope.user).then(function () {
                $scope.user.user_name = "";
                $scope.user.password = "";
                $state.go("main-app.newfeed");
              }, function () {
                $ionicPopup.alert({
                  title: 'Lỗi đăng nhập',
                  template: 'Sai tài khoản hoặc mật khẩu vui lòng kiểm tra lại.'
                }).then(function(res) {
                  $scope.user.password = "";
                });

              });
            };

            $scope.loginFacebook = function () {
                FacebookApi.login();
            };

            $scope.view = {
              api: Api.getServerUrl()
            };

            $scope.configApi = function () {
              $ionicModal.fromTemplateUrl("angular/main-app/config/config.html", {
                scope: $scope,
                animation: "slide-in-up"
              }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
              });
            };


            $scope.saveAddress = function () {
              Api.setServerUrl($scope.view.api);
              $scope.modal.hide();
            }

        })

    ;

})();
